<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddProductAndPerson extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => 'required|string|max:150',
            'parent_category' => 'required|string|max:150',
            'product_name' => 'required|string|max:150|min:4|unique:products,name',
            'product_title' => 'required|string|max:150|min:4',
            'product_number' => 'required|integer|min:0|max:300',
            'product_caption' => 'required|min:4',
            'location_address'=>'required|max:255|min:2|string',
            'city_name' => 'required|max:150',
            'product_price' => 'required|max:150|min:3',
        ];
    }
}
