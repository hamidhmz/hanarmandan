<?php

namespace App\Http\Controllers;

use App\City;
use App\ParentCategory;
use App\Product;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Category;

class SearchController extends Controller
{
    public function showResult($city, $slug = null , Request $request)
    {
        $force = $slug;
        global $slug;
        $slug = $force;
        $force1 = $city;
        global $city;
        $city = $force1;
        cookie('city', $city, 10080);
        $parent_categories = ParentCategory::orderBy('order', 'ASC')->get();
        $categories = Category::orderBy('order', 'ASC')->get();
        $cities = City::orderBy('name', 'ASC')->get();
        $heigh_prices = Product::groupby('price')->orderBy('price', 'DESC')->first();
        $lower_prices = Product::groupby('price')->orderBy('price', 'ASC')->first();
        $products = new Product;
        $products = $products->whereHas('city', function ($query) {$query->where('name',  $GLOBALS['city']);});
        if (null !== ($request->get('upper')) && ($request->get('upper')) != '') {
            $products = $products->where('price', '<=', $request->get('upper'));
        }
        if (null !== ($request->get('lower')) && ($request->get('lower')) != '') {
            $products = $products->where('price', '>=', $request->get('lower'));
        }
        if (null !== $slug) {
            if (Product::whereHas('category', function ($query) { $query->where('slug', $GLOBALS['slug']);})->count() > 0) {
                $products = $products->whereHas('category', function ($query) {
                    $query->where('slug', $GLOBALS['slug']);
                });
            }else{
                $products = $products->whereHas('parent_category', function ($query) {
                    $query->where('slug', $GLOBALS['slug']);
                });
            }
        }
        if(null !== ($request->get('width_search')) && ($request->get('width_search')) != ''){
            if (Product::where('name','%', $request->get('width_search'))->count() > 0){
                $products = $products->where('name',$request->get('width_search'));
            }
            if (Product::whereHas('category', function ($query) {$query->where('name',  $GLOBALS['request']->get('width_search'));})->count() > 0){
                $products = $products->whereHas('category', function ($query) {$query->where('name',  $GLOBALS['request']->get('width_search'));});
            }
            if (Product::where('caption', $request->get('width_search'))->count() > 0){
                $products = $products->where('caption',$request->get('width_search'));
            }
            if (Product::where('title', $request->get('width_search'))->count() > 0){
                $products = $products->where('title',$request->get('width_search'));
            }
        }
        if(null !== ($request->get('price')) && ($request->get('price')) != ''){
            $products = $products->where('price',$request->get('price'));
        }
        if(null !== ($request->get('producer')) && ($request->get('producer')) != ''){
            $products = $products->where('user_id',$request->get('producer'));
        }
        $products = $products->where('status', 1)->where('delete',0);
        $products = $products->paginate(16);
        if (isset($_GET['page']) && $_GET['page'] != '') {
            if ($_GET['page'] > $products->lastPage()) return redirect(url()->current());
        }
        return view('site.search', compact('cities', 'categories', 'city', 'products', 'parent_categories', 'lower_prices', 'heigh_prices'));
    }
}
