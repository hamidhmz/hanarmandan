<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    public function specifyNumberPhone(){
        $cities = City::orderBy('name','ASC')->get();
        return view('user_profile.auth',compact('cities'));
    }
}
