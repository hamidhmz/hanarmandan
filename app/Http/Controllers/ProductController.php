<?php

namespace App\Http\Controllers;

use App\City;
use App\ParentCategory;
use App\Product;
use App\User;
use Hekmatinasser\Verta\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use TCG\Voyager\Models\Category;
use App\Http\Requests;

class ProductController extends Controller
{
    /**
     * @return int
     */
    public function hight()
    {
        return 300;
    }

    /**
     * @return int
     */
    public function width()
    {
        return 300;
    }

    /**
     * @param $id
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showDetails($id, $slug)
    {
        $cities = City::orderBy('name', 'ASC')->get();
        $parent_categories = ParentCategory::orderBy('order', 'ASC')->get();
        $categories = Category::orderBy('order', 'ASC')->get();
        $product = Product::find($id);
        if (!isset($product->id)){
            session()->flash('fail', __('validation.flash.illegal_product'));
            return redirect(url('/'));
        }
        if ($product->slug != $slug){
            session()->flash('fail', __('validation.flash.illegal_product'));
            return redirect(url('/'));
        }
        if ($product->status == 0 || $product->delete == 1) {
            session()->flash('fail', __('validation.flash.illegal_product'));
            return redirect(url('/'));
        }
        $products = Product::where('status',1)->where('delete',0)->where('parent_category_id',$product->parent_category_id)->get();
        return view('site.details', compact('cities', 'product', 'parent_categories' ,'categories' ,'products'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function preview($slug)
    {
        $cities = City::orderBy('name', 'ASC')->get();
        $product = Product::where('slug', $slug)->first();
        $v = new Verta(Verta::createTimestamp($product->create_at));
        return view('user_profile.preview', compact('cities', 'product', 'v'));
    }

    public function storeFirstCategoryShow()
    {
        $cities = City::orderBy('name', 'ASC')->get();
        $parent_categories = ParentCategory::orderBy('order', 'ASC')->get();
        return view('user_profile.category1', compact('cities', 'parent_categories'));
    }

    /**
     * @param $parent_category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function storeSecondCategoryShow($parent_category)
    {
        $id = ParentCategory::where('slug', $parent_category)->first()->id;
        $cities = City::orderBy('name', 'ASC')->get();
        $categories = Category::where('parent_category_id', $id)->orderBy('order', 'ASC')->get();
        return view('user_profile.category2', compact('cities', 'categories', 'parent_category'));
    }

    /**
     * @param $parent_category
     * @param $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function storeShow($parent_category, $category)
    {
        $cities = City::orderBy('name', 'ASC')->get();
        return view('user_profile.new_product', compact('cities', 'category', 'parent_category'));
    }

    /**
     * @param Requests\AddProductAndPerson $request
     */
    public function createNewProduct(Requests\AddProductAndPerson $request)
    {
        if (auth()->check()) {
            $user_id = auth()->user()->id;
            $category_id = Category::where('name', $request->post('category'))->first()->id;
            $parent_category_id = ParentCategory::where('name', $request->post('parent_category'))->first()->id;
            $city_id = City::where('name', $request->post('city_name'))->first()->id;
            $this->imageValidator($request->all())->validate();
            $this->videoValidator($request->all())->validate();
            $name1 = 0;
            $name2 = 0;
            $name3 = 0;
            $name4 = 0;
            $name5 = 0;
            $pic1 = $request->file('pic1');
            $pic2 = $request->file('pic2');
            $pic3 = $request->file('pic3');
            $pic4 = $request->file('pic4');
            $video = $request->file('video');
            if ($pic1 !== null) {
                $name1 = 'pic1_' . time() . '.' . $pic1->getClientOriginalExtension();
                $destinationPath1 = public_path('storage\\img\\users\\userid_' . $user_id);
                $final1 = $pic1->move($destinationPath1, $name1);
                if (!file_exists($destinationPath1 . '\\thumbnail') && !is_dir($destinationPath1 . '\\thumbnail')) {
                    mkdir($destinationPath1 . '\\thumbnail');
                }
                $copyPath1 = $destinationPath1 . '\\' . $name1;
                $img = Image::make($copyPath1);
                $img->resize($this->hight(), $this->width(), function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath1 . '\\thumbnail\\' . $name1);
            }

            if ($pic2 !== null) {
                $name2 = 'pic2_' . time() . '.' . $pic2->getClientOriginalExtension();
                $destinationPath2 = public_path('storage\\img\\users\\userid_' . $user_id);
                $final2 = $pic2->move($destinationPath2, $name2);
                if (!file_exists($destinationPath2 . '\\thumbnail') && !is_dir($destinationPath2 . '\\thumbnail')) {
                    mkdir($destinationPath2 . '\\thumbnail');
                }
                $copyPath2 = $destinationPath2 . '\\' . $name2;
                $img = Image::make($copyPath2);
                $img->resize($this->hight(), $this->width(), function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath2 . '\\thumbnail\\' . $name2);
            }

            if ($pic3 !== null) {
                $name3 = 'pic3_' . time() . '.' . $pic3->getClientOriginalExtension();
                $destinationPath3 = public_path('storage\\img\\users\\userid_' . $user_id);
                $final3 = $pic3->move($destinationPath3, $name3);
                if (!file_exists($destinationPath3 . '\\thumbnail') && !is_dir($destinationPath3 . '\\thumbnail')) {
                    mkdir($destinationPath3 . '\\thumbnail');
                }
                $copyPath3 = $destinationPath3 . '\\' . $name3;
                $img = Image::make($copyPath3);
                $img->resize($this->hight(), $this->width(), function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath3 . '\\thumbnail\\' . $name3);
            }

            if ($pic4 !== null) {
                $name4 = 'pic4_' . time() . '.' . $pic4->getClientOriginalExtension();
                $destinationPath4 = public_path('storage\\img\\users\\userid_' . $user_id);
                $final4 = $pic4->move($destinationPath4, $name4);
                if (!file_exists($destinationPath4 . '\\thumbnail') && !is_dir($destinationPath4 . '\\thumbnail')) {
                    mkdir($destinationPath4 . '\\thumbnail');
                }
                $copyPath4 = $destinationPath4 . '\\' . $name4;
                $img = Image::make($copyPath4);
                $img->resize($this->hight(), $this->width(), function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath4 . '\\thumbnail\\' . $name4);
            }

            if ($video !== null) {
                $name5 = 'video_' . time() . '.' . $video->getClientOriginalExtension();
                $destinationPath5 = public_path('storage\\video\\users\\userid_' . $user_id);
                $final5 = $video->move($destinationPath5, $name5);
            }

            $product = new Product;
            $product->name = $request->post('product_name');
            $product->category_id = $category_id;
            $product->parent_category_id = $parent_category_id;
            $product->city_id = $city_id;
            $product->user_id = $user_id;
            $product->location_address = $request->post('location_address');
            $product->caption = $request->post('product_caption');
            $product->number = $request->post('product_number');
            $product->title = $request->post('product_title');
            $product->price = $request->post('product_price');
            $product->pic1 = $name1;
            $product->pic2 = $name2;
            $product->pic3 = $name3;
            $product->pic4 = $name4;
            $product->video = $name5;
            if ($product->save()) {
                return redirect('/store/preview/' . $product->slug);
            }
        } else {
            $this->validator($request->all())->validate();
            $user_id = $this->registerNewUser($request);
            Auth::loginUsingId($user_id);
            $category_id = Category::where('name', $request->post('category'))->first()->id;
            $parent_category_id = ParentCategory::where('name', $request->post('parent_category'))->first()->id;
            $city_id = City::where('name', $request->post('city_name'))->first()->id;
            $this->imageValidator($request->all())->validate();
            $this->videoValidator($request->all())->validate();
            $name1 = 0;
            $name2 = 0;
            $name3 = 0;
            $name4 = 0;
            $name5 = 0;
            $pic1 = $request->file('pic1');
            $pic2 = $request->file('pic2');
            $pic3 = $request->file('pic3');
            $pic4 = $request->file('pic4');
            $video = $request->file('video');
            if ($pic1 !== null) {
                $name1 = 'pic1_' . time() . '.' . $pic1->getClientOriginalExtension();
                $destinationPath1 = public_path('storage\\img\\users\\userid_' . $user_id);
                $final1 = $pic1->move($destinationPath1, $name1);
                if (!file_exists($destinationPath1 . '\\thumbnail') && !is_dir($destinationPath1 . '\\thumbnail')) {
                    mkdir($destinationPath1 . '\\thumbnail');
                }
                $copyPath1 = $destinationPath1 . '\\' . $name1;
                $img = Image::make($copyPath1);
                $img->resize($this->hight(), $this->width(), function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath1 . '\\thumbnail\\' . $name1);
            }

            if ($pic2 !== null) {
                $name2 = 'pic2_' . time() . '.' . $pic2->getClientOriginalExtension();
                $destinationPath2 = public_path('storage\\img\\users\\userid_' . $user_id);
                $final2 = $pic2->move($destinationPath2, $name2);
                if (!file_exists($destinationPath2 . '\\thumbnail') && !is_dir($destinationPath2 . '\\thumbnail')) {
                    mkdir($destinationPath2 . '\\thumbnail');
                }
                $copyPath2 = $destinationPath2 . '\\' . $name2;
                $img = Image::make($copyPath2);
                $img->resize($this->hight(), $this->width(), function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath2 . '\\thumbnail\\' . $name2);
            }

            if ($pic3 !== null) {
                $name3 = 'pic3_' . time() . '.' . $pic3->getClientOriginalExtension();
                $destinationPath3 = public_path('storage\\img\\users\\userid_' . $user_id);
                $final3 = $pic3->move($destinationPath3, $name3);
                if (!file_exists($destinationPath3 . '\\thumbnail') && !is_dir($destinationPath3 . '\\thumbnail')) {
                    mkdir($destinationPath3 . '\\thumbnail');
                }
                $copyPath3 = $destinationPath3 . '\\' . $name3;
                $img = Image::make($copyPath3);
                $img->resize($this->hight(), $this->width(), function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath3 . '\\thumbnail\\' . $name3);
            }

            if ($pic4 !== null) {
                $name4 = 'pic4_' . time() . '.' . $pic4->getClientOriginalExtension();
                $destinationPath4 = public_path('storage\\img\\users\\userid_' . $user_id);
                $final4 = $pic4->move($destinationPath4, $name4);
                if (!file_exists($destinationPath4 . '\\thumbnail') && !is_dir($destinationPath4 . '\\thumbnail')) {
                    mkdir($destinationPath4 . '\\thumbnail');
                }
                $copyPath4 = $destinationPath4 . '\\' . $name4;
                $img = Image::make($copyPath4);
                $img->resize($this->hight(), $this->width(), function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath4 . '\\thumbnail\\' . $name4);
            }

            if ($video !== null) {
                $name5 = 'video_' . time() . '.' . $video->getClientOriginalExtension();
                $destinationPath5 = public_path('storage\\video\\users\\userid_' . $user_id);
                $final5 = $video->move($destinationPath5, $name5);
            }

            $product = new Product;
            $product->name = $request->post('product_name');
            $product->category_id = $category_id;
            $product->parent_category_id = $parent_category_id;
            $product->city_id = $city_id;
            $product->user_id = $user_id;
            $product->location_address = $request->post('location_address');
            $product->caption = $request->post('product_caption');
            $product->number = $request->post('product_number');
            $product->title = $request->post('product_title');
            $product->price = $request->post('product_price');
            $product->pic1 = $name1;
            $product->pic2 = $name2;
            $product->pic3 = $name3;
            $product->pic4 = $name4;
            $product->video = $name5;
            if ($product->save()) {
                return redirect('/store/preview/' . $product->slug);
            }
        }
    }

    /**
     * @param $request
     * @return mixed
     */
    protected function registerNewUser($request)
    {
        $user = new User;
        $user->name = $request->post('name');
        $user->role_id = 2;
        $user->password = bcrypt($request->post('password'));
        $user->phone_number = $request->post('phone_number');
        $user->family_name = $request->post('family_name');
        $user->site_address = $request->post('site_address');
        $user->telegram_id = $request->post('telegram_id');
        $user->email = $request->post('email');
        $user->custom_design_and_production = $request->post('custom_design_and_production');
        $user->teaching_and_training = $request->post('teaching_and_training');
        $user->e_learning = $request->post('e_learning');
        $user->art_degree = $request->post('art_degree');
        $user->teach_learning_plan = $request->post('teach_learning_plan');
        $user->save();
        return $user->id;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deletePic1($id)
    {
        $product = Product::find($id);
        if (auth()->user()->id == $product->user_id) {
            $this->deletePic1Function($id);
            return back();
        } else {
            return back();
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deletePic2($id)
    {
        $product = Product::find($id);
        if (auth()->user()->id == $product->user_id) {
            $this->deletePic2Function($id);
            return back();
        } else {
            return back();
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deletePic3($id)
    {
        $product = Product::find($id);
        if (auth()->user()->id == $product->user_id) {
            $this->deletePic3Function($id);
            return back();
        } else {
            return back();
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deletePic4($id)
    {
        $product = Product::find($id);
        if (auth()->user()->id == $product->user_id) {
            $this->deletePic4Function($id);
            return back();
        } else {
            return back();
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteVideo($id)
    {
        $product = Product::find($id);
        if (auth()->user()->id == $product->user_id) {
            $this->deleteVideoFunction($id);
            return back();
        } else {
            return back();
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteProduct($id)
    {
        $product = Product::find($id);
        echo $product->user_id;
        echo '<br>';
        echo auth()->user()->id;
        if (auth()->user()->id == $product->user_id) {
            $product->delete = 1;
            $product->save();
            session()->flash('done', __('validation.flash.done'));
            return redirect('/');
        } else {
            session()->flash('fail', __('validation.flash.without_premission'));
            return back();
        }
    }

    /**
     * @param $id
     * @param $request
     * @throws \Illuminate\Validation\ValidationException
     */
    public function editDetails($id, Request $request)
    {
        $user_id = auth()->user()->id;
        $this->imageValidator($request->all())->validate();
        $this->videoValidator($request->all())->validate();
        $this->productValidator($request->all())->validate();
        $product = Product::find($id);
        if (auth()->user()->id != $product->user_id){
            session()->flash('fail',__('validation.flash.without_premission'));
            return back();
        }
        $city_id = City::where('name', $request->post('city_name'))->first()->id;
        $name1 = $product->pic1;
        $name2 = $product->pic2;
        $name3 = $product->pic3;
        $name4 = $product->pic4;
        $name5 = $product->video;
        $pic1 = $request->file('pic1');
        $pic2 = $request->file('pic2');
        $pic3 = $request->file('pic3');
        $pic4 = $request->file('pic4');
        $video = $request->file('video');
        if ($pic1 !== null) {
            $this->deletePic1Function($id);
            $name1 = 'pic1_' . time() . '.' . $pic1->getClientOriginalExtension();
            $destinationPath1 = public_path('storage\\img\\users\\userid_' . $user_id);
            $final1 = $pic1->move($destinationPath1, $name1);
            if (!file_exists($destinationPath1 . '\\thumbnail') && !is_dir($destinationPath1 . '\\thumbnail')) {
                mkdir($destinationPath1 . '\\thumbnail');
            }
            $copyPath1 = $destinationPath1 . '\\' . $name1;
            $img = Image::make($copyPath1);
            $img->resize($this->hight(), $this->width(), function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath1 . '\\thumbnail\\' . $name1);
        }

        if ($pic2 !== null) {
            $this->deletePic2Function($id);
            $name2 = 'pic2_' . time() . '.' . $pic2->getClientOriginalExtension();
            $destinationPath2 = public_path('storage\\img\\users\\userid_' . $user_id);
            $final2 = $pic2->move($destinationPath2, $name2);
            if (!file_exists($destinationPath2 . '\\thumbnail') && !is_dir($destinationPath2 . '\\thumbnail')) {
                mkdir($destinationPath2 . '\\thumbnail');
            }
            $copyPath2 = $destinationPath2 . '\\' . $name2;
            $img = Image::make($copyPath2);
            $img->resize($this->hight(), $this->width(), function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath2 . '\\thumbnail\\' . $name2);
        }

        if ($pic3 !== null) {
            $this->deletePic3Function($id);
            $name3 = 'pic3_' . time() . '.' . $pic3->getClientOriginalExtension();
            $destinationPath3 = public_path('storage\\img\\users\\userid_' . $user_id);
            $final3 = $pic3->move($destinationPath3, $name3);
            if (!file_exists($destinationPath3 . '\\thumbnail') && !is_dir($destinationPath3 . '\\thumbnail')) {
                mkdir($destinationPath3 . '\\thumbnail');
            }
            $copyPath3 = $destinationPath3 . '\\' . $name3;
            $img = Image::make($copyPath3);
            $img->resize($this->hight(), $this->width(), function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath3 . '\\thumbnail\\' . $name3);
        }

        if ($pic4 !== null) {
            $this->deletePic4Function($id);
            $name4 = 'pic4_' . time() . '.' . $pic4->getClientOriginalExtension();
            $destinationPath4 = public_path('storage\\img\\users\\userid_' . $user_id);
            $final4 = $pic4->move($destinationPath4, $name4);
            if (!file_exists($destinationPath4 . '\\thumbnail') && !is_dir($destinationPath4 . '\\thumbnail')) {
                mkdir($destinationPath4 . '\\thumbnail');
            }
            $copyPath4 = $destinationPath4 . '\\' . $name4;
            $img = Image::make($copyPath4);
            $img->resize($this->hight(), $this->width(), function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath4 . '\\thumbnail\\' . $name4);
        }

        if ($video !== null) {
            $this->deleteVideoFunction($id);
            $name5 = 'video_' . time() . '.' . $video->getClientOriginalExtension();
            $destinationPath5 = public_path('storage\\video\\users\\userid_' . $user_id);
            $final5 = $video->move($destinationPath5, $name5);
        }
        $product->title = $request->post('product_title');
        $product->number = $request->post('product_number');
        $product->caption = $request->post('product_caption');
        $product->location_address = $request->post('location_address');
        $product->city_id = $city_id;
        $product->price = $request->post('product_price');
        $product->pic1 = $name1;
        $product->pic2 = $name2;
        $product->pic3 = $name3;
        $product->pic4 = $name4;
        $product->video = $name5;
        $product->save();
        return back();
    }


    /**
     * @param array $data
     * @return \Illuminate\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'phone_number' => 'required|string|min:7|max:80|unique:users',
            'family_name' => 'required|string|min:4|max:80',
            'custom_design_and_production' => 'required|integer|min:0|max:1',
            'teaching_and_training' => 'required|integer|min:0|max:1',
            'e_learning' => 'required|integer|min:0|max:1',
            'art_degree' => 'required|integer|min:0|max:1',
            'teach_learning_plan' => 'required|integer|min:0|max:1',
        ]);
    }

    /**
     * @param array $data
     * @return \Illuminate\Validation\Validator
     */
    protected function productValidator(array $data)
    {
        return Validator::make($data, [
            'product_title' => 'required|string|max:150|min:4',
            'product_number' => 'required|integer|min:0|max:300',
            'product_caption' => 'required|min:4',
            'location_address' => 'required|max:255|min:2|string',
            'city_name' => 'required|max:150',
            'product_price' => 'required|max:150|min:3',
        ]);
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function imageValidator(array $data)
    {
        return Validator::make($data, [
            'pic1' => 'image',
            'pic2' => 'image',
            'pic3' => 'image',
            'pic4' => 'image',
        ]);
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function videoValidator(array $data)
    {
        return Validator::make($data, [
            'video' => 'mimetypes:video/avi,video/mpeg,video/quicktime,video/x-flv,video/mp4,application/x-mpegURL,video/MP2T,video/3gpp,video/x-msvideo,video/x-ms-wmv|max:20000'
        ]);
    }

    /**
     * @param $id
     */
    public function deletePic1Function($id)
    {
        $product = Product::find($id);
        $picture = $product->pic1;
        $product->pic1 = '0';
        if ($product->save()) {
            if (is_dir(public_path('storage\\img\\users\\userid_' . auth()->user()->id . '\\thumbnail\\' . $picture))) {
                unlink(public_path('storage\\img\\users\\userid_' . auth()->user()->id . '\\thumbnail\\' . $picture));
            }
            if (is_dir(public_path('storage\\img\\users\\userid_' . auth()->user()->id . '\\' . $picture))) {
                unlink(public_path('storage\\img\\users\\userid_' . auth()->user()->id . '\\' . $picture));
            }
            session()->flash('done', __('validation.flash.done'));
        } else {
            session()->flash('fail', __('validation.flash.fail'));
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deletePic2Function($id)
    {
        $product = Product::find($id);
        $picture = $product->pic2;
        $product->pic2 = '0';
        if ($product->save()) {
            if (is_dir(public_path('storage\\img\\users\\userid_' . auth()->user()->id . '\\thumbnail\\' . $picture))) {
                unlink(public_path('storage\\img\\users\\userid_' . auth()->user()->id . '\\thumbnail\\' . $picture));
            }
            if (is_dir(public_path('storage\\img\\users\\userid_' . auth()->user()->id . '\\' . $picture))) {
                unlink(public_path('storage\\img\\users\\userid_' . auth()->user()->id . '\\' . $picture));
            }
            session()->flash('done', __('validation.flash.done'));
        } else {
            session()->flash('fail', __('validation.flash.fail'));
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deletePic3Function($id)
    {
        $product = Product::find($id);
        $picture = $product->pic3;
        $product->pic3 = '0';
        if ($product->save()) {
            if (is_dir(public_path('storage\\img\\users\\userid_' . auth()->user()->id . '\\thumbnail\\' . $picture))) {
                unlink(public_path('storage\\img\\users\\userid_' . auth()->user()->id . '\\thumbnail\\' . $picture));
            }
            if (is_dir(public_path('storage\\img\\users\\userid_' . auth()->user()->id . '\\' . $picture))) {
                unlink(public_path('storage\\img\\users\\userid_' . auth()->user()->id . '\\' . $picture));
            }
            session()->flash('done', __('validation.flash.done'));
        } else {
            session()->flash('fail', __('validation.flash.fail'));
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deletePic4Function($id)
    {
        $product = Product::find($id);
        $picture = $product->pic4;
        $product->pic4 = '0';
        if ($product->save()) {
            if (is_dir(public_path('storage\\img\\users\\userid_' . auth()->user()->id . '\\thumbnail\\' . $picture))) {
                unlink(public_path('storage\\img\\users\\userid_' . auth()->user()->id . '\\thumbnail\\' . $picture));
            }
            if (is_dir(public_path('storage\\img\\users\\userid_' . auth()->user()->id . '\\' . $picture))) {
                unlink(public_path('storage\\img\\users\\userid_' . auth()->user()->id . '\\' . $picture));
            }
            session()->flash('done', __('validation.flash.done'));
        } else {
            session()->flash('fail', __('validation.flash.fail'));
        }
    }

    /**
     * @param $id
     */
    public function deleteVideoFunction($id)
    {
        $product = Product::find($id);
        $video = $product->video;
        $product->video = '0';
        if ($product->save()) {
            if (is_dir(public_path('storage\\video\\users\\userid_' . auth()->user()->id . '\\' . $video))) {
                unlink(public_path('storage\\video\\users\\userid_' . auth()->user()->id . '\\' . $video));
            }
            session()->flash('done', __('validation.flash.done'));
        } else {
            session()->flash('fail', __('validation.flash.fail'));
        }
    }
}

