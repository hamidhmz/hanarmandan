<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Requests;

class CommentController extends Controller
{
    /**
     * @param Requests\CheckCommentText $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createNewComment(Requests\CheckCommentText $request, $id)
    {
        $product = Product::find($id);
        if ($product->delete == 1 || $product->status == 0) {
            session()->flash('fail', __('validation.flash.without_premission'));
            return back();
        }
        $userId = auth()->user()->id;
        $product->comments()->create([
            'context' => $request->post('context'),
            'user_id' => $userId,
        ]);
        session()->flash('done', __('validation.flash.done'));
        return back();
    }
}
