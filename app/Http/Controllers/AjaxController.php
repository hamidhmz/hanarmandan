<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    protected function passAjaxRequest(Request $request){
        $products = new Product;
        if (null !== ($request->get('upper')) && ($request->get('upper')) != '') {
            $products = $products->where('price', '<=d', $request->get('upper'));
        }
        if (null !== ($request->get('lower')) && ($request->get('lower')) != '' ) {
            $products = $products->where('price', '>=', $request->get('lower'));
        }
        $products = $products->paginate(3);$products = Product::paginate(3);
        return view('site.ajax.result',compact('products'));
    }
}
