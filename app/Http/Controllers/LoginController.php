<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function previewLoginForm(){
        $cities = City::orderBy('name', 'ASC')->get();
        return view('site.login', compact('cities'));
    }
}
