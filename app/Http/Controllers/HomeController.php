<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $cities = City::orderBy('name','ASC')->get();
        return view('site.home',compact('cities'));
    }
}
