<?php

namespace App\Http\Controllers;

use App\City;
use App\User;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function showShopToOthers($slug)
    {
        $user = User::where('name' ,str_replace('-',' ',$slug))->first();
        $products = $user->products()->get();
        $cities = City::orderBy('name', 'ASC')->get();
        return view('site.shop', compact('user' ,'products' ,'cities'));
    }
}
