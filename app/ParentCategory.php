<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class ParentCategory extends Model
{
    use Sluggable;
    protected $fillable = [
        'name',
        'slug',
        'order',
    ];

    /**
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    public function categories()
    {
        return $this->hasMany('TCG\Voyager\Models\Category');
    }
}
