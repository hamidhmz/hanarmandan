var filter = document.getElementById('filter');
var list = document.getElementById('list');
var listItems = list.querySelectorAll('li');

//filter.focus();

filter.addEventListener('keyup', function(e) {
  var val = new RegExp(e.target.value, 'gi');
  for(var i=0; i<listItems.length; i++) {
    if( e.target.value.length > 0) {
      var text = listItems[i].innerHTML;
    
      if( !text.match(val)) {
        listItems[i].classList.add('hidden');
      } else {
        listItems[i].classList.remove('hidden');
      }
    } else {
      listItems[i].classList.remove('hidden');
    }
    
  }
});
  
  var filter2 = document.getElementById('filter2');
var list2 = document.getElementById('list2');
var listItems2 = list2.querySelectorAll('li');

//filter2.focus();

filter2.addEventListener('keyup', function(e) {
  var val = new RegExp(e.target.value, 'gi');
  for(var i=0; i<listItems2.length; i++) {
    if( e.target.value.length > 0) {
      var text = listItems2[i].innerHTML;
    
      if( !text.match(val)) {
        listItems2[i].classList.add('hidden');
      } else {
        listItems2[i].classList.remove('hidden');
      }
    } else {
      listItems2[i].classList.remove('hidden');
    }
    
  }
});
  
$(function () { $("[data-toggle = 'tooltip']").tooltip(); });