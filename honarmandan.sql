-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 25, 2018 at 01:39 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `honarmandan`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_category_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_category_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(9, 2, 1, 'تابلوفرش', 'تابلوفرش', '2018-02-17 03:08:40', '2018-02-17 03:08:40'),
(10, 2, 2, 'گبه بافی', 'گبه-بافی', '2018-02-17 03:09:00', '2018-02-17 03:09:00'),
(11, 2, 3, 'گلیم بافی', 'گلیم-بافی', '2018-02-17 03:09:13', '2018-02-17 03:09:13'),
(12, 2, 4, 'گزلیم بافی', 'گزلیم-بافی', '2018-02-17 03:09:24', '2018-02-17 03:09:42'),
(13, 2, 5, 'یراق بافی', 'یراق-بافی', '2018-02-17 03:09:58', '2018-02-17 03:09:58'),
(14, 2, 6, 'پوشاک نمدمالی', 'پوشاک-نمدمالی', '2018-02-17 03:10:09', '2018-02-17 03:10:09'),
(15, 2, 7, 'تاپستری', 'تاپستری', '2018-02-17 03:10:21', '2018-02-17 03:10:29'),
(16, 2, 8, 'قلاب بافی', 'قلاب-بافی', '2018-02-17 03:10:43', '2018-02-17 03:10:52'),
(17, 2, 9, 'ورنی بافی', 'ورنی-بافی', '2018-02-17 03:11:40', '2018-02-17 03:11:40'),
(19, 2, 10, 'زیورالات بافتنی', 'زیورالات-بافتنی', '2018-02-17 03:12:39', '2018-02-17 03:12:39'),
(30, 3, 1, 'قلمزنی', 'قلمزنی', '2018-02-17 04:14:49', '2018-02-17 04:14:49'),
(31, 3, 2, 'ملیله کاری', 'ملیله-کاری', '2018-02-17 04:16:56', '2018-02-17 04:16:56'),
(32, 3, 3, 'نقره سازی', 'نقره-سازی', '2018-02-17 04:17:20', '2018-02-17 04:17:20'),
(33, 3, 4, 'زیورالات سنتی', 'زیورالات-سنتی', '2018-02-17 04:17:50', '2018-02-17 04:17:50'),
(34, 3, 5, 'تزیینات فلزی مشبک', 'تزیینات-فلزی-مشبک', '2018-02-17 04:18:02', '2018-02-17 04:18:02'),
(35, 4, 1, 'خاتم کار', 'خاتم-کار', '2018-02-17 04:18:13', '2018-02-17 04:18:13'),
(36, 4, 2, 'مشبک کار', 'مشبک-کار', '2018-02-17 04:18:27', '2018-02-17 04:18:27'),
(37, 4, 3, 'منبت کار', 'منبت-کار', '2018-02-17 04:18:37', '2018-02-17 04:18:37'),
(38, 4, 4, 'زیورالات چوبی', 'زیورالات-چوبی', '2018-02-17 04:18:50', '2018-02-17 04:18:50'),
(39, 4, 5, 'جعبه وصندوقچه های دست ساز', 'جعبه-وصندوقچه-های-دست-ساز', '2018-02-17 04:19:03', '2018-02-17 04:19:03'),
(40, 4, 6, 'معرق کار', 'معرق-کار', '2018-02-17 04:19:14', '2018-02-17 04:19:14'),
(41, 5, 1, 'نقاشی روی استخوان', 'نقاشی-روی-استخوان', '2018-02-17 04:19:26', '2018-02-17 04:19:26'),
(42, 5, 2, 'نقاشی وحکاکی روی صابون', 'نقاشی-وحکاکی-روی-صابون', '2018-02-17 04:19:41', '2018-02-17 04:19:41'),
(43, 5, 3, 'مینا نقاشی', 'مینا-نقاشی', '2018-02-17 04:19:54', '2018-02-17 04:19:54'),
(44, 5, 4, 'خوشنویسی', 'خوشنویسی', '2018-02-17 04:20:07', '2018-02-17 04:20:07'),
(45, 5, 5, 'نگارگری', 'نگارگری', '2018-02-17 04:20:23', '2018-02-17 04:20:23'),
(46, 5, 6, 'رنگ وروغن', 'رنگ-وروغن', '2018-02-17 04:20:36', '2018-02-17 04:20:36'),
(47, 5, 7, 'ابرنگ', 'ابرنگ', '2018-02-17 04:20:53', '2018-02-17 04:20:53'),
(48, 5, 8, 'پاستل', 'پاستل', '2018-02-17 04:21:07', '2018-02-17 04:21:07'),
(49, 5, 9, 'سیاه قلم', 'سیاه-قلم', '2018-02-17 04:21:20', '2018-02-17 04:21:20'),
(50, 5, 10, 'باتیک', 'باتیک', '2018-02-17 04:21:36', '2018-02-17 04:21:36'),
(51, 5, 11, 'چاپ باتیک', 'چاپ-باتیک', '2018-02-17 04:21:49', '2018-02-17 04:21:49'),
(52, 5, 12, 'چاپ قلمکار', 'چاپ-قلمکار', '2018-02-17 04:22:02', '2018-02-17 04:22:02'),
(53, 5, 13, 'پاپیه ماشه', 'پاپیه-ماشه', '2018-02-17 04:22:17', '2018-02-17 04:22:17'),
(54, 5, 14, 'طراحی سنتی', 'طراحی-سنتی', '2018-02-17 04:22:30', '2018-02-17 04:22:30'),
(57, 6, 1, 'حکاکی روی چرم', 'حکاکی-روی-چرم', '2018-02-17 04:48:39', '2018-02-17 04:48:39'),
(58, 6, 2, 'نقاشی روی چرم', 'نقاشی-روی-چرم', '2018-02-17 04:48:58', '2018-02-17 04:48:58'),
(59, 6, 3, 'معرق چرم', 'معرق-چرم', '2018-02-17 04:49:13', '2018-02-17 04:49:13'),
(60, 6, 4, 'کمربند دستکش', 'کمربند-دستکش', '2018-02-17 04:49:30', '2018-02-17 04:49:30'),
(61, 6, 5, 'زیورالات چرمی', 'زیورالات-چرمی', '2018-02-17 04:49:48', '2018-02-17 04:49:48'),
(62, 6, 6, 'کیفهای چرمی', 'کیفهای-چرمی', '2018-02-17 04:50:07', '2018-02-17 04:50:07'),
(63, 6, 7, 'کفش های دست ساز', 'کفش-های-دست-ساز', '2018-02-17 04:50:26', '2018-02-17 04:50:26'),
(64, 6, 8, 'زیراندازهای چرمینه', 'زیراندازهای-چرمینه', '2018-02-17 04:50:46', '2018-02-17 04:50:46'),
(65, 7, 1, 'سرمه دوزی', 'سرمه-دوزی', '2018-02-17 04:51:15', '2018-02-17 04:51:15'),
(66, 7, 2, 'پته دوزی', 'پته-دوزی', '2018-02-17 04:51:33', '2018-02-17 04:51:33'),
(67, 7, 3, 'ابریشم دوزی', 'ابریشم-دوزی', '2018-02-17 04:51:56', '2018-02-17 04:51:56'),
(68, 7, 4, 'بلوچ دوزی', 'بلوچ-دوزی', '2018-02-17 04:52:16', '2018-02-17 04:52:16'),
(69, 7, 5, 'نقده دوزی', 'نقده-دوزی', '2018-02-17 04:52:36', '2018-02-17 04:52:36'),
(70, 7, 6, 'چهل وتکه دوزی', 'چهل-وتکه-دوزی', '2018-02-17 04:53:02', '2018-02-17 04:53:02'),
(71, 7, 7, 'ربان دوزی', 'ربان-دوزی', '2018-02-17 04:53:23', '2018-02-23 06:46:18'),
(72, 7, 8, 'زیورالات تزیینی', 'زیورالات-تزیینی', '2018-02-17 04:53:45', '2018-02-23 06:44:54'),
(73, 7, 9, 'ملزومات تزیینی خانگی', 'ملزومات-تزیینی-خانگی', '2018-02-17 04:54:26', '2018-02-23 06:44:21'),
(74, 7, 10, 'نمدمالی سوزنی', 'نمدمالی-سوزنی', '2018-02-17 04:54:55', '2018-02-23 06:43:40'),
(75, 7, 11, 'پوشاک سنتی', 'پوشاک-سنتی', '2018-02-17 04:55:14', '2018-02-23 06:43:03'),
(76, 8, 1, 'تراش سنگ', 'تراش-سنگ', '2018-02-17 04:55:53', '2018-02-23 06:42:36'),
(77, 8, 2, 'نقاشی روی سفال', 'نقاشی-روی-سفال', '2018-02-17 04:56:27', '2018-02-23 06:42:15'),
(78, 8, 3, 'حکاکی روی سفال', 'حکاکی-روی-سفال', '2018-02-17 05:34:47', '2018-02-23 06:41:57'),
(79, 8, 4, 'فیروزه کوبی', 'فیروزه-کوبی', '2018-02-17 05:35:28', '2018-02-23 06:41:32'),
(80, 8, 5, 'حکاکی روی سنگ نمک', 'حکاکی-روی-سنگ-نمک', '2018-02-17 05:36:09', '2018-02-23 06:40:32'),
(81, 8, 6, 'حکاکی روی سنگ های قیمتی', 'حکاکی-روی-سنگ-های-قیمتی', '2018-02-17 05:36:47', '2018-02-23 06:39:59'),
(82, 8, 7, 'حکاکی روی شیشه', 'حکاکی-روی-شیشه', '2018-02-17 05:37:32', '2018-02-23 06:37:07'),
(83, 8, 8, 'نقاشی روی شیشه', 'نقاشی-روی-شیشه', '2018-02-17 05:37:58', '2018-02-23 06:36:43'),
(84, 8, 9, 'سرامیک', 'سرامیک', '2018-02-17 05:38:31', '2018-02-23 06:35:06');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `province_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `province_id`, `created_at`, `updated_at`) VALUES
(1, 'آذر شهر', '1', NULL, NULL),
(2, 'اسكو', '1', NULL, NULL),
(3, 'اهر', '1', NULL, NULL),
(4, 'بستان آباد', '1', NULL, NULL),
(5, 'بناب', '1', NULL, NULL),
(6, 'بندر شرفخانه', '1', NULL, NULL),
(7, 'تبريز', '1', NULL, NULL),
(8, 'تسوج', '1', NULL, NULL),
(9, 'جلفا', '1', NULL, NULL),
(10, 'سراب', '1', NULL, NULL),
(11, 'شبستر', '1', NULL, NULL),
(12, 'صوفیان', '1', NULL, NULL),
(13, 'عجبشير', '1', NULL, NULL),
(14, 'قره آغاج', '1', NULL, NULL),
(15, 'كليبر', '1', NULL, NULL),
(16, 'كندوان', '1', NULL, NULL),
(17, 'مراغه', '1', NULL, NULL),
(18, 'مرند', '1', NULL, NULL),
(19, 'ملكان', '1', NULL, NULL),
(20, 'ممقان', '1', NULL, NULL),
(21, 'ميانه', '1', NULL, NULL),
(22, 'هاديشهر', '1', NULL, NULL),
(23, 'هريس', '1', NULL, NULL),
(24, 'هشترود', '1', NULL, NULL),
(25, 'ورزقان', '1', NULL, NULL),
(26, 'اروميه', '2', NULL, NULL),
(27, 'اشنويه', '2', NULL, NULL),
(28, 'بازرگان', '2', NULL, NULL),
(29, 'بوكان', '2', NULL, NULL),
(30, 'پلدشت', '2', NULL, NULL),
(31, 'پيرانشهر', '2', NULL, NULL),
(32, 'تكاب', '2', NULL, NULL),
(33, 'خوي', '2', NULL, NULL),
(34, 'سردشت', '2', NULL, NULL),
(35, 'سلماس', '2', NULL, NULL),
(36, 'سيه چشمه- چالدران', '2', NULL, NULL),
(37, 'سیمینه', '2', NULL, NULL),
(38, 'شاهين دژ', '2', NULL, NULL),
(39, 'شوط', '2', NULL, NULL),
(40, 'ماكو', '2', NULL, NULL),
(41, 'مهاباد', '2', NULL, NULL),
(42, 'مياندوآب', '2', NULL, NULL),
(43, 'نقده', '2', NULL, NULL),
(44, 'اردبيل', '3', NULL, NULL),
(45, 'بيله سوار', '3', NULL, NULL),
(46, 'پارس آباد', '3', NULL, NULL),
(47, 'خلخال', '3', NULL, NULL),
(48, 'سرعين', '3', NULL, NULL),
(49, 'كيوي (كوثر)', '3', NULL, NULL),
(50, 'گرمي (مغان)', '3', NULL, NULL),
(51, 'مشگين شهر', '3', NULL, NULL),
(52, 'مغان (سمنان)', '3', NULL, NULL),
(53, 'نمين', '3', NULL, NULL),
(54, 'نير', '3', NULL, NULL),
(55, 'آران و بيدگل', '4', NULL, NULL),
(56, 'اردستان', '4', NULL, NULL),
(57, 'اصفهان', '4', NULL, NULL),
(58, 'باغ بهادران', '4', NULL, NULL),
(59, 'تيران', '4', NULL, NULL),
(60, 'خميني شهر', '4', NULL, NULL),
(61, 'خوانسار', '4', NULL, NULL),
(62, 'دهاقان', '4', NULL, NULL),
(63, 'دولت آباد-اصفهان', '4', NULL, NULL),
(64, 'زرين شهر', '4', NULL, NULL),
(65, 'زيباشهر (محمديه)', '4', NULL, NULL),
(66, 'سميرم', '4', NULL, NULL),
(67, 'شاهين شهر', '4', NULL, NULL),
(68, 'شهرضا', '4', NULL, NULL),
(69, 'فريدن', '4', NULL, NULL),
(70, 'فريدون شهر', '4', NULL, NULL),
(71, 'فلاورجان', '4', NULL, NULL),
(72, 'فولاد شهر', '4', NULL, NULL),
(73, 'قهدریجان', '4', NULL, NULL),
(74, 'كاشان', '4', NULL, NULL),
(75, 'گلپايگان', '4', NULL, NULL),
(76, 'گلدشت اصفهان', '4', NULL, NULL),
(77, 'گلدشت مركزی', '4', NULL, NULL),
(78, 'مباركه اصفهان', '4', NULL, NULL),
(79, 'مهاباد-اصفهان', '4', NULL, NULL),
(80, 'نايين', '4', NULL, NULL),
(81, 'نجف آباد', '4', NULL, NULL),
(82, 'نطنز', '4', NULL, NULL),
(83, 'هرند', '4', NULL, NULL),
(84, 'آسارا', '5', NULL, NULL),
(85, 'اشتهارد', '5', NULL, NULL),
(86, 'شهر جديد هشتگرد', '5', NULL, NULL),
(87, 'طالقان', '5', NULL, NULL),
(88, 'كرج', '5', NULL, NULL),
(89, 'گلستان تهران', '5', NULL, NULL),
(90, 'نظرآباد', '5', NULL, NULL),
(91, 'هشتگرد', '5', NULL, NULL),
(92, 'آبدانان', '6', NULL, NULL),
(93, 'ايلام', '6', NULL, NULL),
(94, 'ايوان', '6', NULL, NULL),
(95, 'دره شهر', '6', NULL, NULL),
(96, 'دهلران', '6', NULL, NULL),
(97, 'سرابله', '6', NULL, NULL),
(98, 'شيروان چرداول', '6', NULL, NULL),
(99, 'مهران', '6', NULL, NULL),
(100, 'آبپخش', '7', NULL, NULL),
(101, 'اهرم', '7', NULL, NULL),
(102, 'برازجان', '7', NULL, NULL),
(103, 'بندر دير', '7', NULL, NULL),
(104, 'بندر ديلم', '7', NULL, NULL),
(105, 'بندر كنگان', '7', NULL, NULL),
(106, 'بندر گناوه', '7', NULL, NULL),
(107, 'بوشهر', '7', NULL, NULL),
(108, 'تنگستان', '7', NULL, NULL),
(109, 'جزيره خارك', '7', NULL, NULL),
(110, 'جم (ولايت)', '7', NULL, NULL),
(111, 'خورموج', '7', NULL, NULL),
(112, 'دشتستان - شبانکاره', '7', NULL, NULL),
(113, 'دلوار', '7', NULL, NULL),
(114, 'عسلویه', '7', NULL, NULL),
(115, 'اسلامشهر', '8', NULL, NULL),
(116, 'بومهن', '8', NULL, NULL),
(117, 'پاكدشت', '8', NULL, NULL),
(118, 'تهران', '8', NULL, NULL),
(119, 'چهاردانگه', '8', NULL, NULL),
(120, 'دماوند', '8', NULL, NULL),
(121, 'رودهن', '8', NULL, NULL),
(122, 'ري', '8', NULL, NULL),
(123, 'شريف آباد', '8', NULL, NULL),
(124, 'شهر رباط كريم', '8', NULL, NULL),
(125, 'شهر شهريار', '8', NULL, NULL),
(126, 'فشم', '8', NULL, NULL),
(127, 'فيروزكوه', '8', NULL, NULL),
(128, 'قدس', '8', NULL, NULL),
(129, 'كهريزك', '8', NULL, NULL),
(130, 'لواسان بزرگ', '8', NULL, NULL),
(131, 'ملارد', '8', NULL, NULL),
(132, 'ورامين', '8', NULL, NULL),
(133, 'اردل', '9', NULL, NULL),
(134, 'بروجن', '9', NULL, NULL),
(135, 'چلگرد (كوهرنگ)', '9', NULL, NULL),
(136, 'سامان', '9', NULL, NULL),
(137, 'شهركرد', '9', NULL, NULL),
(138, 'فارسان', '9', NULL, NULL),
(139, 'لردگان', '9', NULL, NULL),
(140, 'بشرویه', '10', NULL, NULL),
(141, 'بيرجند', '10', NULL, NULL),
(142, 'خضری', '10', NULL, NULL),
(143, 'خوسف', '10', NULL, NULL),
(144, 'سرایان', '10', NULL, NULL),
(145, 'سربيشه', '10', NULL, NULL),
(146, 'طبس', '10', NULL, NULL),
(147, 'فردوس', '10', NULL, NULL),
(148, 'قائن', '10', NULL, NULL),
(149, 'نهبندان', '10', NULL, NULL),
(150, 'بجستان', '11', NULL, NULL),
(151, 'بردسكن', '11', NULL, NULL),
(152, 'تايباد', '11', NULL, NULL),
(153, 'تربت جام', '11', NULL, NULL),
(154, 'تربت حيدريه', '11', NULL, NULL),
(155, 'جغتای', '11', NULL, NULL),
(156, 'جوین', '11', NULL, NULL),
(157, 'چناران', '11', NULL, NULL),
(158, 'خلیل آباد', '11', NULL, NULL),
(159, 'خواف', '11', NULL, NULL),
(160, 'درگز', '11', NULL, NULL),
(161, 'رشتخوار', '11', NULL, NULL),
(162, 'سبزوار', '11', NULL, NULL),
(163, 'سرخس', '11', NULL, NULL),
(164, 'طبس', '11', NULL, NULL),
(165, 'طرقبه', '11', NULL, NULL),
(166, 'فريمان', '11', NULL, NULL),
(167, 'قوچان', '11', NULL, NULL),
(168, 'كاشمر', '11', NULL, NULL),
(169, 'كلات', '11', NULL, NULL),
(170, 'گناباد', '11', NULL, NULL),
(171, 'مشهد', '11', NULL, NULL),
(172, 'نيشابور', '11', NULL, NULL),
(173, 'آشخانه، مانه و سمرقان', '12', NULL, NULL),
(174, 'اسفراين', '12', NULL, NULL),
(175, 'بجنورد', '12', NULL, NULL),
(176, 'جاجرم', '12', NULL, NULL),
(177, 'شيروان', '12', NULL, NULL),
(178, 'فاروج', '12', NULL, NULL),
(179, 'آبادان', '13', NULL, NULL),
(180, 'اميديه', '13', NULL, NULL),
(181, 'انديمشك', '13', NULL, NULL),
(182, 'اهواز', '13', NULL, NULL),
(183, 'ايذه', '13', NULL, NULL),
(184, 'باغ ملك', '13', NULL, NULL),
(185, 'بستان', '13', NULL, NULL),
(186, 'بندر ماهشهر', '13', NULL, NULL),
(187, 'بندرامام خميني', '13', NULL, NULL),
(188, 'بهبهان', '13', NULL, NULL),
(189, 'خرمشهر', '13', NULL, NULL),
(190, 'دزفول', '13', NULL, NULL),
(191, 'رامشیر', '13', NULL, NULL),
(192, 'رامهرمز', '13', NULL, NULL),
(193, 'سوسنگرد', '13', NULL, NULL),
(194, 'شادگان', '13', NULL, NULL),
(195, 'شوش', '13', NULL, NULL),
(196, 'شوشتر', '13', NULL, NULL),
(197, 'لالي', '13', NULL, NULL),
(198, 'مسجد سليمان', '13', NULL, NULL),
(199, 'هنديجان', '13', NULL, NULL),
(200, 'هويزه', '13', NULL, NULL),
(201, 'آب بر (طارم)', '14', NULL, NULL),
(202, 'ابهر', '14', NULL, NULL),
(203, 'خرمدره', '14', NULL, NULL),
(204, 'زرین آباد (ایجرود)', '14', NULL, NULL),
(205, 'زنجان', '14', NULL, NULL),
(206, 'قیدار (خدا بنده)', '14', NULL, NULL),
(207, 'ماهنشان', '14', NULL, NULL),
(208, 'ايوانكي', '15', NULL, NULL),
(209, 'بسطام', '15', NULL, NULL),
(210, 'دامغان', '15', NULL, NULL),
(211, 'سرخه', '15', NULL, NULL),
(212, 'سمنان', '15', NULL, NULL),
(213, 'شاهرود', '15', NULL, NULL),
(214, 'شهمیرزاد', '15', NULL, NULL),
(215, 'گرمسار', '15', NULL, NULL),
(216, 'مهدیشهر', '15', NULL, NULL),
(217, 'ايرانشهر', '16', NULL, NULL),
(218, 'چابهار', '16', NULL, NULL),
(219, 'خاش', '16', NULL, NULL),
(220, 'راسك', '16', NULL, NULL),
(221, 'زابل', '16', NULL, NULL),
(222, 'زاهدان', '16', NULL, NULL),
(223, 'سراوان', '16', NULL, NULL),
(224, 'سرباز', '16', NULL, NULL),
(225, 'ميرجاوه', '16', NULL, NULL),
(226, 'نيكشهر', '16', NULL, NULL),
(227, 'آباده', '17', NULL, NULL),
(228, 'آباده طشك', '17', NULL, NULL),
(229, 'اردكان', '17', NULL, NULL),
(230, 'ارسنجان', '17', NULL, NULL),
(231, 'استهبان', '17', NULL, NULL),
(232, 'اشكنان', '17', NULL, NULL),
(233, 'اقليد', '17', NULL, NULL),
(234, 'اوز', '17', NULL, NULL),
(235, 'ایج', '17', NULL, NULL),
(236, 'ایزد خواست', '17', NULL, NULL),
(237, 'باب انار', '17', NULL, NULL),
(238, 'بالاده', '17', NULL, NULL),
(239, 'بنارويه', '17', NULL, NULL),
(240, 'بهمن', '17', NULL, NULL),
(241, 'بوانات', '17', NULL, NULL),
(242, 'بيرم', '17', NULL, NULL),
(243, 'بیضا', '17', NULL, NULL),
(244, 'جنت شهر', '17', NULL, NULL),
(245, 'جهرم', '17', NULL, NULL),
(246, 'حاجي آباد-زرین دشت', '17', NULL, NULL),
(247, 'خاوران', '17', NULL, NULL),
(248, 'خرامه', '17', NULL, NULL),
(249, 'خشت', '17', NULL, NULL),
(250, 'خفر', '17', NULL, NULL),
(251, 'خنج', '17', NULL, NULL),
(252, 'خور', '17', NULL, NULL),
(253, 'داراب', '17', NULL, NULL),
(254, 'رونيز عليا', '17', NULL, NULL),
(255, 'زاهدشهر', '17', NULL, NULL),
(256, 'زرقان', '17', NULL, NULL),
(257, 'سده', '17', NULL, NULL),
(258, 'سروستان', '17', NULL, NULL),
(259, 'سعادت شهر', '17', NULL, NULL),
(260, 'سورمق', '17', NULL, NULL),
(261, 'ششده', '17', NULL, NULL),
(262, 'شيراز', '17', NULL, NULL),
(263, 'صغاد', '17', NULL, NULL),
(264, 'صفاشهر', '17', NULL, NULL),
(265, 'علاء مرودشت', '17', NULL, NULL),
(266, 'عنبر', '17', NULL, NULL),
(267, 'فراشبند', '17', NULL, NULL),
(268, 'فسا', '17', NULL, NULL),
(269, 'فيروز آباد', '17', NULL, NULL),
(270, 'قائميه', '17', NULL, NULL),
(271, 'قادر آباد', '17', NULL, NULL),
(272, 'قطب آباد', '17', NULL, NULL),
(273, 'قير', '17', NULL, NULL),
(274, 'كازرون', '17', NULL, NULL),
(275, 'كنار تخته', '17', NULL, NULL),
(276, 'گراش', '17', NULL, NULL),
(277, 'لار', '17', NULL, NULL),
(278, 'لامرد', '17', NULL, NULL),
(279, 'لپوئی', '17', NULL, NULL),
(280, 'لطيفي', '17', NULL, NULL),
(281, 'مبارك آباد ديز', '17', NULL, NULL),
(282, 'مرودشت', '17', NULL, NULL),
(283, 'مشكان', '17', NULL, NULL),
(284, 'مصير', '17', NULL, NULL),
(285, 'مهر فارس(گله دار)', '17', NULL, NULL),
(286, 'ميمند', '17', NULL, NULL),
(287, 'نوبندگان', '17', NULL, NULL),
(288, 'نودان', '17', NULL, NULL),
(289, 'نورآباد', '17', NULL, NULL),
(290, 'ني ريز', '17', NULL, NULL),
(291, 'کوار', '17', NULL, NULL),
(292, 'آبيك', '18', NULL, NULL),
(293, 'البرز', '18', NULL, NULL),
(294, 'بوئين زهرا', '18', NULL, NULL),
(295, 'تاكستان', '18', NULL, NULL),
(296, 'قزوين', '18', NULL, NULL),
(297, 'محمود آباد نمونه', '18', NULL, NULL),
(298, 'قم', '19', NULL, NULL),
(299, 'بانه', '20', NULL, NULL),
(300, 'بيجار', '20', NULL, NULL),
(301, 'دهگلان', '20', NULL, NULL),
(302, 'ديواندره', '20', NULL, NULL),
(303, 'سقز', '20', NULL, NULL),
(304, 'سنندج', '20', NULL, NULL),
(305, 'قروه', '20', NULL, NULL),
(306, 'كامياران', '20', NULL, NULL),
(307, 'مريوان', '20', NULL, NULL),
(308, 'بابك', '21', NULL, NULL),
(309, 'بافت', '21', NULL, NULL),
(310, 'بردسير', '21', NULL, NULL),
(311, 'بم', '21', NULL, NULL),
(312, 'جيرفت', '21', NULL, NULL),
(313, 'راور', '21', NULL, NULL),
(314, 'رفسنجان', '21', NULL, NULL),
(315, 'زرند', '21', NULL, NULL),
(316, 'سيرجان', '21', NULL, NULL),
(317, 'كرمان', '21', NULL, NULL),
(318, 'كهنوج', '21', NULL, NULL),
(319, 'منوجان', '21', NULL, NULL),
(320, 'اسلام آباد غرب', '22', NULL, NULL),
(321, 'پاوه', '22', NULL, NULL),
(322, 'تازه آباد- ثلاث باباجانی', '22', NULL, NULL),
(323, 'جوانرود', '22', NULL, NULL),
(324, 'سر پل ذهاب', '22', NULL, NULL),
(325, 'سنقر كليائي', '22', NULL, NULL),
(326, 'صحنه', '22', NULL, NULL),
(327, 'قصر شيرين', '22', NULL, NULL),
(328, 'كرمانشاه', '22', NULL, NULL),
(329, 'كنگاور', '22', NULL, NULL),
(330, 'گيلان غرب', '22', NULL, NULL),
(331, 'هرسين', '22', NULL, NULL),
(332, 'دهدشت', '23', NULL, NULL),
(333, 'دوگنبدان', '23', NULL, NULL),
(334, 'سي سخت- دنا', '23', NULL, NULL),
(335, 'گچساران', '23', NULL, NULL),
(336, 'ياسوج', '23', NULL, NULL),
(337, 'آزاد شهر', '24', NULL, NULL),
(338, 'آق قلا', '24', NULL, NULL),
(339, 'انبارآلوم', '24', NULL, NULL),
(340, 'اینچه برون', '24', NULL, NULL),
(341, 'بندر گز', '24', NULL, NULL),
(342, 'تركمن', '24', NULL, NULL),
(343, 'جلین', '24', NULL, NULL),
(344, 'خان ببین', '24', NULL, NULL),
(345, 'راميان', '24', NULL, NULL),
(346, 'سرخس کلاته', '24', NULL, NULL),
(347, 'سیمین شهر', '24', NULL, NULL),
(348, 'علي آباد كتول', '24', NULL, NULL),
(349, 'فاضل آباد', '24', NULL, NULL),
(350, 'كردكوي', '24', NULL, NULL),
(351, 'كلاله', '24', NULL, NULL),
(352, 'گالیکش', '24', NULL, NULL),
(353, 'گرگان', '24', NULL, NULL),
(354, 'گمیش تپه', '24', NULL, NULL),
(355, 'گنبد كاووس', '24', NULL, NULL),
(356, 'مراوه تپه', '24', NULL, NULL),
(357, 'مينو دشت', '24', NULL, NULL),
(358, 'نگین شهر', '24', NULL, NULL),
(359, 'نوده خاندوز', '24', NULL, NULL),
(360, 'نوکنده', '24', NULL, NULL),
(361, 'آستارا', '25', NULL, NULL),
(362, 'آستانه اشرفيه', '25', NULL, NULL),
(363, 'املش', '25', NULL, NULL),
(364, 'بندرانزلي', '25', NULL, NULL),
(365, 'خمام', '25', NULL, NULL),
(366, 'رشت', '25', NULL, NULL),
(367, 'رضوان شهر', '25', NULL, NULL),
(368, 'رود سر', '25', NULL, NULL),
(369, 'رودبار', '25', NULL, NULL),
(370, 'سياهكل', '25', NULL, NULL),
(371, 'شفت', '25', NULL, NULL),
(372, 'صومعه سرا', '25', NULL, NULL),
(373, 'فومن', '25', NULL, NULL),
(374, 'كلاچاي', '25', NULL, NULL),
(375, 'لاهيجان', '25', NULL, NULL),
(376, 'لنگرود', '25', NULL, NULL),
(377, 'لوشان', '25', NULL, NULL),
(378, 'ماسال', '25', NULL, NULL),
(379, 'ماسوله', '25', NULL, NULL),
(380, 'منجيل', '25', NULL, NULL),
(381, 'هشتپر', '25', NULL, NULL),
(382, 'ازنا', '26', NULL, NULL),
(383, 'الشتر', '26', NULL, NULL),
(384, 'اليگودرز', '26', NULL, NULL),
(385, 'بروجرد', '26', NULL, NULL),
(386, 'پلدختر', '26', NULL, NULL),
(387, 'خرم آباد', '26', NULL, NULL),
(388, 'دورود', '26', NULL, NULL),
(389, 'سپید دشت', '26', NULL, NULL),
(390, 'كوهدشت', '26', NULL, NULL),
(391, 'نورآباد (خوزستان)', '26', NULL, NULL),
(392, 'آمل', '27', NULL, NULL),
(393, 'بابل', '27', NULL, NULL),
(394, 'بابلسر', '27', NULL, NULL),
(395, 'بلده', '27', NULL, NULL),
(396, 'بهشهر', '27', NULL, NULL),
(397, 'پل سفيد', '27', NULL, NULL),
(398, 'تنكابن', '27', NULL, NULL),
(399, 'جويبار', '27', NULL, NULL),
(400, 'چالوس', '27', NULL, NULL),
(401, 'خرم آباد', '27', NULL, NULL),
(402, 'رامسر', '27', NULL, NULL),
(403, 'رستم کلا', '27', NULL, NULL),
(404, 'ساري', '27', NULL, NULL),
(405, 'سلمانشهر', '27', NULL, NULL),
(406, 'سواد كوه', '27', NULL, NULL),
(407, 'فريدون كنار', '27', NULL, NULL),
(408, 'قائم شهر', '27', NULL, NULL),
(409, 'گلوگاه', '27', NULL, NULL),
(410, 'محمودآباد', '27', NULL, NULL),
(411, 'مرزن آباد', '27', NULL, NULL),
(412, 'نكا', '27', NULL, NULL),
(413, 'نور', '27', NULL, NULL),
(414, 'نوشهر', '27', NULL, NULL),
(415, 'آشتيان', '28', NULL, NULL),
(416, 'اراك', '28', NULL, NULL),
(417, 'تفرش', '28', NULL, NULL),
(418, 'خمين', '28', NULL, NULL),
(419, 'دليجان', '28', NULL, NULL),
(420, 'ساوه', '28', NULL, NULL),
(421, 'شازند', '28', NULL, NULL),
(422, 'محلات', '28', NULL, NULL),
(423, 'کمیجان', '28', NULL, NULL),
(424, 'ابوموسي', '29', NULL, NULL),
(425, 'انگهران', '29', NULL, NULL),
(426, 'بستك', '29', NULL, NULL),
(427, 'بندر جاسك', '29', NULL, NULL),
(428, 'بندر لنگه', '29', NULL, NULL),
(429, 'بندرعباس', '29', NULL, NULL),
(430, 'پارسیان', '29', NULL, NULL),
(431, 'حاجي آباد', '29', NULL, NULL),
(432, 'دشتی', '29', NULL, NULL),
(433, 'دهبارز (رودان)', '29', NULL, NULL),
(434, 'قشم', '29', NULL, NULL),
(435, 'كيش', '29', NULL, NULL),
(436, 'ميناب', '29', NULL, NULL),
(437, 'اسدآباد', '30', NULL, NULL),
(438, 'بهار', '30', NULL, NULL),
(439, 'تويسركان', '30', NULL, NULL),
(440, 'رزن', '30', NULL, NULL),
(441, 'كبودر اهنگ', '30', NULL, NULL),
(442, 'ملاير', '30', NULL, NULL),
(443, 'نهاوند', '30', NULL, NULL),
(444, 'همدان', '30', NULL, NULL),
(445, 'ابركوه', '31', NULL, NULL),
(446, 'اردكان', '31', NULL, NULL),
(447, 'اشكذر', '31', NULL, NULL),
(448, 'بافق', '31', NULL, NULL),
(449, 'تفت', '31', NULL, NULL),
(450, 'مهريز', '31', NULL, NULL),
(451, 'ميبد', '31', NULL, NULL),
(452, 'هرات', '31', NULL, NULL),
(453, 'يزد', '31', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clips`
--

CREATE TABLE `clips` (
  `id` int(10) UNSIGNED NOT NULL,
  `clip_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `context` longtext COLLATE utf8_unicode_ci,
  `user_id` int(11) NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `context`, `user_id`, `product_id`, `comment_id`, `created_at`, `updated_at`) VALUES
(1, 'تهه', 1, 36, NULL, '2018-04-23 16:45:23', '2018-04-23 16:45:23'),
(2, 'شکل طراحي دسته بر روي مچ\r\n طراحي جذاب\r\n خلاف نظر برخي دوستان کارايي راحت\r\nبسیار شیگ و با کفیته از نظر وزن مشکلی نداره خیلی خوب رو مچ سوار میشه انقدر راحته که شب ها میتونی باش بخوابی(اونایی که عادت ندارن) اصلاً آزار دهنده نیست.', 11, 46, NULL, '2018-04-23 18:27:28', '2018-04-23 18:27:28');

-- --------------------------------------------------------

--
-- Table structure for table `curriculums`
--

CREATE TABLE `curriculums` (
  `id` int(10) UNSIGNED NOT NULL,
  `pdf` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caption` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(2, 1, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, '', 2),
(3, 1, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, '', 3),
(4, 1, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '', 4),
(5, 1, 'excerpt', 'text_area', 'excerpt', 1, 0, 1, 1, 1, 1, '', 5),
(6, 1, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '', 6),
(7, 1, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(8, 1, 'slug', 'text', 'slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 8),
(9, 1, 'meta_description', 'text_area', 'meta_description', 1, 0, 1, 1, 1, 1, '', 9),
(10, 1, 'meta_keywords', 'text_area', 'meta_keywords', 1, 0, 1, 1, 1, 1, '', 10),
(11, 1, 'status', 'select_dropdown', 'status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(12, 1, 'created_at', 'timestamp', 'created_at', 0, 1, 1, 0, 0, 0, '', 12),
(13, 1, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 13),
(14, 2, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(15, 2, 'author_id', 'text', 'author_id', 1, 0, 0, 0, 0, 0, '', 2),
(16, 2, 'title', 'text', 'title', 1, 1, 1, 1, 1, 1, '', 3),
(17, 2, 'excerpt', 'text_area', 'excerpt', 1, 0, 1, 1, 1, 1, '', 4),
(18, 2, 'body', 'rich_text_box', 'body', 1, 0, 1, 1, 1, 1, '', 5),
(19, 2, 'slug', 'text', 'slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"}}', 6),
(20, 2, 'meta_description', 'text', 'meta_description', 1, 0, 1, 1, 1, 1, '', 7),
(21, 2, 'meta_keywords', 'text', 'meta_keywords', 1, 0, 1, 1, 1, 1, '', 8),
(22, 2, 'status', 'select_dropdown', 'status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(23, 2, 'created_at', 'timestamp', 'created_at', 1, 1, 1, 0, 0, 0, '', 10),
(24, 2, 'updated_at', 'timestamp', 'updated_at', 1, 0, 0, 0, 0, 0, '', 11),
(25, 2, 'image', 'image', 'image', 0, 1, 1, 1, 1, 1, '', 12),
(26, 3, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(27, 3, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, '', 2),
(28, 3, 'email', 'text', 'email', 1, 1, 1, 1, 1, 1, '', 3),
(29, 3, 'password', 'password', 'password', 0, 0, 0, 1, 1, 0, '', 4),
(30, 3, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}', 10),
(31, 3, 'remember_token', 'text', 'remember_token', 0, 0, 0, 0, 0, 0, '', 5),
(32, 3, 'created_at', 'timestamp', 'created_at', 0, 1, 1, 0, 0, 0, '', 6),
(33, 3, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 7),
(34, 3, 'avatar', 'image', 'avatar', 0, 1, 1, 1, 1, 1, '', 8),
(35, 5, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(36, 5, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, '', 2),
(37, 5, 'created_at', 'timestamp', 'created_at', 0, 0, 0, 0, 0, 0, '', 3),
(38, 5, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 4),
(39, 4, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, NULL, 1),
(40, 4, 'parent_id', 'select_dropdown', 'parent_id', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(41, 4, 'order', 'text', 'order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(42, 4, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|unique:categories\"}}', 4),
(43, 4, 'slug', 'text', 'slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"},\"validation\":{\"rules\":\"unique:categories\"}}', 5),
(44, 4, 'created_at', 'timestamp', 'created_at', 0, 0, 1, 0, 0, 0, NULL, 6),
(45, 4, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, NULL, 7),
(46, 6, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(47, 6, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(48, 6, 'created_at', 'timestamp', 'created_at', 0, 0, 0, 0, 0, 0, '', 3),
(49, 6, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 4),
(50, 6, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '', 5),
(51, 1, 'seo_title', 'text', 'seo_title', 0, 1, 1, 1, 1, 1, '', 14),
(52, 1, 'featured', 'checkbox', 'featured', 1, 1, 1, 1, 1, 1, '', 15),
(53, 3, 'role_id', 'text', 'role_id', 1, 1, 1, 1, 1, 1, '', 9),
(55, 7, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(56, 7, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 2),
(57, 7, 'category_id', 'text', 'Category Id', 0, 1, 1, 1, 1, 1, NULL, 3),
(58, 7, 'city_id', 'text', 'City Id', 0, 1, 1, 1, 1, 1, NULL, 4),
(59, 7, 'user_id', 'text', 'User Id', 0, 1, 1, 1, 1, 1, NULL, 5),
(60, 7, 'caption', 'text_area', 'Caption', 0, 1, 1, 1, 1, 1, NULL, 6),
(61, 7, 'number', 'text', 'Number', 0, 1, 1, 1, 1, 1, NULL, 7),
(62, 7, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, NULL, 8),
(63, 7, 'price', 'text', 'Price', 0, 1, 1, 1, 1, 1, NULL, 9),
(64, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 10),
(65, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 11),
(66, 7, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, NULL, 12),
(67, 7, 'status', 'select_dropdown', 'Status', 0, 0, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"false\",\"1\":\"true\"}}', 13),
(68, 7, 'ladder', 'checkbox', 'Ladder', 0, 0, 1, 1, 1, 1, '{\"on\":\"فعال\",\"off\":\"غیر فعال\",\"checked\":\"true\"}', 14),
(69, 7, 'urgent', 'checkbox', 'Urgent', 0, 0, 1, 1, 1, 1, '{\"on\":\"فعال\",\"off\":\"غیر فعال\",\"checked\":\"true\"}', 15),
(70, 7, 'ladder_urgent', 'checkbox', 'Ladder Urgent', 0, 0, 1, 1, 1, 1, '{\"on\":\"فعال\",\"off\":\"غیر فعال\",\"checked\":\"true\"}', 16),
(71, 7, 'continuation', 'checkbox', 'Continuation', 0, 0, 1, 1, 1, 1, '{\"on\":\"فعال\",\"off\":\"غیر فعال\",\"checked\":\"true\"}', 17),
(72, 7, 'ladder_continuation', 'checkbox', 'Ladder Continuation', 0, 0, 1, 1, 1, 1, '{\"on\":\"فعال\",\"off\":\"غیر فعال\",\"checked\":\"true\"}', 18),
(73, 7, 'website_link', 'checkbox', 'Website Link', 0, 0, 1, 1, 1, 1, '{\"on\":\"فعال\",\"off\":\"غیر فعال\",\"checked\":\"true\"}', 19),
(74, 7, 'expert', 'checkbox', 'Expert', 0, 0, 1, 1, 1, 1, '{\"on\":\"فعال\",\"off\":\"غیر فعال\",\"checked\":\"true\"}', 20),
(75, 7, 'product_belongsto_category_relationship', 'relationship', 'category', 0, 1, 1, 1, 1, 1, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Category\",\"table\":\"categories\",\"type\":\"belongsTo\",\"column\":\"category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\"}', 21),
(76, 7, 'product_belongsto_user_relationship', 'relationship', 'user', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\"}', 22),
(77, 7, 'product_belongsto_city_relationship', 'relationship', 'city', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\City\",\"table\":\"cities\",\"type\":\"belongsTo\",\"column\":\"city_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\"}', 23),
(79, 8, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(80, 8, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 2),
(81, 8, 'order', 'text', 'Order', 0, 1, 1, 1, 1, 1, NULL, 3),
(82, 8, 'slug', 'text', 'Slug', 0, 1, 1, 0, 0, 1, '{\"slugify\":{\"origin\":\"name\"},\"validation\":{\"rules\":\"unique:parent_categories\"}}', 4),
(83, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 5),
(84, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
(85, 7, 'product_belongsto_parent_category_relationship', 'relationship', 'parent_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\ParentCategory\",\"table\":\"parent_categories\",\"type\":\"belongsTo\",\"column\":\"parent_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\"}', 24),
(86, 7, 'parent_category_id', 'checkbox', 'Parent Category Id', 0, 1, 1, 1, 1, 1, NULL, 21),
(88, 9, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(89, 9, 'context', 'text_area', 'Context', 0, 1, 1, 0, 1, 1, NULL, 3),
(90, 9, 'user_id', 'checkbox', 'User Id', 1, 1, 1, 1, 1, 1, NULL, 2),
(91, 9, 'comment_id', 'checkbox', 'Comment Id', 0, 0, 0, 0, 0, 0, NULL, 4),
(92, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 5),
(93, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
(94, 9, 'comment_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Comment\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\"}', 7);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `created_at`, `updated_at`) VALUES
(1, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, '2018-02-07 05:42:44', '2018-02-07 05:42:44'),
(2, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, '2018-02-07 05:42:44', '2018-02-07 05:42:44'),
(3, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', '', '', 1, 0, '2018-02-07 05:42:44', '2018-02-07 05:42:44'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, NULL, NULL, 1, 0, '2018-02-07 05:42:44', '2018-02-17 02:26:56'),
(5, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, '2018-02-07 05:42:44', '2018-02-07 05:42:44'),
(6, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, '2018-02-07 05:42:44', '2018-02-07 05:42:44'),
(7, 'products', 'products', 'Product', 'Products', 'voyager-archive', 'App\\Product', NULL, NULL, NULL, 1, 0, '2018-02-17 11:09:46', '2018-02-18 03:44:39'),
(8, 'parent_categories', 'parent-categories', 'Parent Category', 'Parent Categories', 'voyager-bolt', 'App\\ParentCategory', NULL, NULL, NULL, 1, 0, '2018-02-23 04:46:10', '2018-02-23 05:35:51'),
(9, 'comments', 'comments', 'Comment', 'Comments', NULL, 'App\\Comment', NULL, NULL, NULL, 1, 0, '2018-04-23 14:02:49', '2018-04-23 14:02:49');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `image_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `materials`
--

CREATE TABLE `materials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2018-02-07 05:42:46', '2018-02-07 05:42:46');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2018-02-07 05:42:46', '2018-02-07 05:42:46', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 4, '2018-02-07 05:42:46', '2018-02-18 03:43:31', 'voyager.media.index', NULL),
(3, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 6, '2018-02-07 05:42:46', '2018-02-07 05:42:46', 'voyager.posts.index', NULL),
(4, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2018-02-07 05:42:46', '2018-02-07 05:42:46', 'voyager.users.index', NULL),
(5, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 9, '2018-02-07 05:42:47', '2018-02-23 05:34:39', 'voyager.categories.index', NULL),
(6, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 7, '2018-02-07 05:42:47', '2018-02-07 05:42:47', 'voyager.pages.index', NULL),
(7, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2018-02-07 05:42:47', '2018-02-07 05:42:47', 'voyager.roles.index', NULL),
(8, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 10, '2018-02-07 05:42:47', '2018-02-23 05:34:39', NULL, NULL),
(9, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 8, 1, '2018-02-07 05:42:47', '2018-02-18 03:43:31', 'voyager.menus.index', NULL),
(10, 1, 'Database', '', '_self', 'voyager-data', NULL, 8, 2, '2018-02-07 05:42:47', '2018-02-18 03:43:31', 'voyager.database.index', NULL),
(11, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 8, 3, '2018-02-07 05:42:47', '2018-02-18 03:43:31', 'voyager.compass.index', NULL),
(12, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 11, '2018-02-07 05:42:47', '2018-02-23 05:34:39', 'voyager.settings.index', NULL),
(13, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 8, 4, '2018-02-07 05:42:52', '2018-02-18 03:43:31', 'voyager.hooks', NULL),
(14, 1, 'Products', '/admin/products', '_self', 'voyager-archive', '#000000', NULL, 5, '2018-02-17 11:09:48', '2018-02-18 03:44:08', NULL, ''),
(15, 1, 'Parent Categories', '/admin/parent-categories', '_self', 'voyager-bolt', '#000000', NULL, 8, '2018-02-23 04:46:10', '2018-02-23 05:35:29', NULL, ''),
(16, 1, 'Comments', '/admin/comments', '_self', NULL, NULL, NULL, 12, '2018-04-23 14:02:50', '2018-04-23 14:02:50', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_01_01_000000_create_pages_table', 1),
(6, '2016_01_01_000000_create_posts_table', 1),
(7, '2016_02_15_204651_create_categories_table', 1),
(8, '2016_05_19_173453_create_menu_table', 1),
(9, '2016_10_21_190000_create_roles_table', 1),
(10, '2016_10_21_190000_create_settings_table', 1),
(11, '2016_11_30_135954_create_permission_table', 1),
(12, '2016_11_30_141208_create_permission_role_table', 1),
(13, '2016_12_26_201236_data_types__add__server_side', 1),
(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(15, '2017_01_14_005015_create_translations_table', 1),
(16, '2017_01_15_000000_add_permission_group_id_to_permissions_table', 1),
(17, '2017_01_15_000000_create_permission_groups_table', 1),
(18, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(19, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(20, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
(21, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(22, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(23, '2017_08_05_000000_add_group_to_settings_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2018-02-07 05:42:50', '2018-02-07 05:42:50');

-- --------------------------------------------------------

--
-- Table structure for table `parent_categories`
--

CREATE TABLE `parent_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `parent_categories`
--

INSERT INTO `parent_categories` (`id`, `name`, `order`, `slug`, `created_at`, `updated_at`) VALUES
(2, 'آثار دست بافت', '1', 'آثار-دست-بافت', '2018-02-23 05:30:15', '2018-02-23 05:30:15'),
(3, 'اثار هنرهای فلزی', '2', 'اثار-هنرهای-فلزی', '2018-02-23 05:31:08', '2018-02-23 05:31:08'),
(4, 'آثارچوبی', '3', 'آثارچوبی', '2018-02-23 05:31:22', '2018-02-23 05:31:22'),
(5, 'آثارهنرهای تزیینی', '4', 'آثارهنرهای-تزیینی', '2018-02-23 05:32:08', '2018-02-23 05:32:08'),
(6, 'اثارچرمی', '5', 'اثارچرمی', '2018-02-23 05:32:36', '2018-02-23 05:32:36'),
(7, 'انواع رودوزی', '6', 'انواع-رودوزی', '2018-02-23 05:32:47', '2018-02-23 05:32:47'),
(8, 'سنگ وسفال شیشه', '7', 'سنگ-وسفال-شیشه', '2018-02-23 05:32:57', '2018-02-23 05:32:57');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permission_group_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`, `permission_group_id`) VALUES
(1, 'browse_admin', NULL, '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(2, 'browse_database', NULL, '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(3, 'browse_media', NULL, '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(4, 'browse_compass', NULL, '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(5, 'browse_menus', 'menus', '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(6, 'read_menus', 'menus', '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(7, 'edit_menus', 'menus', '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(8, 'add_menus', 'menus', '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(9, 'delete_menus', 'menus', '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(10, 'browse_pages', 'pages', '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(11, 'read_pages', 'pages', '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(12, 'edit_pages', 'pages', '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(13, 'add_pages', 'pages', '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(14, 'delete_pages', 'pages', '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(15, 'browse_roles', 'roles', '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(16, 'read_roles', 'roles', '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(17, 'edit_roles', 'roles', '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(18, 'add_roles', 'roles', '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(19, 'delete_roles', 'roles', '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(20, 'browse_users', 'users', '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(21, 'read_users', 'users', '2018-02-07 05:42:47', '2018-02-07 05:42:47', NULL),
(22, 'edit_users', 'users', '2018-02-07 05:42:48', '2018-02-07 05:42:48', NULL),
(23, 'add_users', 'users', '2018-02-07 05:42:48', '2018-02-07 05:42:48', NULL),
(24, 'delete_users', 'users', '2018-02-07 05:42:48', '2018-02-07 05:42:48', NULL),
(25, 'browse_posts', 'posts', '2018-02-07 05:42:48', '2018-02-07 05:42:48', NULL),
(26, 'read_posts', 'posts', '2018-02-07 05:42:48', '2018-02-07 05:42:48', NULL),
(27, 'edit_posts', 'posts', '2018-02-07 05:42:48', '2018-02-07 05:42:48', NULL),
(28, 'add_posts', 'posts', '2018-02-07 05:42:48', '2018-02-07 05:42:48', NULL),
(29, 'delete_posts', 'posts', '2018-02-07 05:42:48', '2018-02-07 05:42:48', NULL),
(30, 'browse_categories', 'categories', '2018-02-07 05:42:48', '2018-02-07 05:42:48', NULL),
(31, 'read_categories', 'categories', '2018-02-07 05:42:48', '2018-02-07 05:42:48', NULL),
(32, 'edit_categories', 'categories', '2018-02-07 05:42:48', '2018-02-07 05:42:48', NULL),
(33, 'add_categories', 'categories', '2018-02-07 05:42:48', '2018-02-07 05:42:48', NULL),
(34, 'delete_categories', 'categories', '2018-02-07 05:42:48', '2018-02-07 05:42:48', NULL),
(35, 'browse_settings', 'settings', '2018-02-07 05:42:48', '2018-02-07 05:42:48', NULL),
(36, 'read_settings', 'settings', '2018-02-07 05:42:48', '2018-02-07 05:42:48', NULL),
(37, 'edit_settings', 'settings', '2018-02-07 05:42:48', '2018-02-07 05:42:48', NULL),
(38, 'add_settings', 'settings', '2018-02-07 05:42:48', '2018-02-07 05:42:48', NULL),
(39, 'delete_settings', 'settings', '2018-02-07 05:42:48', '2018-02-07 05:42:48', NULL),
(40, 'browse_hooks', NULL, '2018-02-07 05:42:52', '2018-02-07 05:42:52', NULL),
(41, 'browse_products', 'products', '2018-02-17 11:09:47', '2018-02-17 11:09:47', NULL),
(42, 'read_products', 'products', '2018-02-17 11:09:47', '2018-02-17 11:09:47', NULL),
(43, 'edit_products', 'products', '2018-02-17 11:09:47', '2018-02-17 11:09:47', NULL),
(44, 'add_products', 'products', '2018-02-17 11:09:47', '2018-02-17 11:09:47', NULL),
(45, 'delete_products', 'products', '2018-02-17 11:09:47', '2018-02-17 11:09:47', NULL),
(46, 'browse_parent_categories', 'parent_categories', '2018-02-23 04:46:10', '2018-02-23 04:46:10', NULL),
(47, 'read_parent_categories', 'parent_categories', '2018-02-23 04:46:10', '2018-02-23 04:46:10', NULL),
(48, 'edit_parent_categories', 'parent_categories', '2018-02-23 04:46:10', '2018-02-23 04:46:10', NULL),
(49, 'add_parent_categories', 'parent_categories', '2018-02-23 04:46:10', '2018-02-23 04:46:10', NULL),
(50, 'delete_parent_categories', 'parent_categories', '2018-02-23 04:46:10', '2018-02-23 04:46:10', NULL),
(51, 'browse_comments', 'comments', '2018-04-23 14:02:49', '2018-04-23 14:02:49', NULL),
(52, 'read_comments', 'comments', '2018-04-23 14:02:49', '2018-04-23 14:02:49', NULL),
(53, 'edit_comments', 'comments', '2018-04-23 14:02:49', '2018-04-23 14:02:49', NULL),
(54, 'add_comments', 'comments', '2018-04-23 14:02:49', '2018-04-23 14:02:49', NULL),
(55, 'delete_comments', 'comments', '2018-04-23 14:02:49', '2018-04-23 14:02:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_groups`
--

CREATE TABLE `permission_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/February2018/43xZjYCC3kQ5Esrn7nPw.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-02-07 05:42:50', '2018-02-18 07:07:58'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-02-07 05:42:50', '2018-02-07 05:42:50'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-02-07 05:42:50', '2018-02-07 05:42:50'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-02-07 05:42:50', '2018-02-07 05:42:50');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `location_address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `caption` text COLLATE utf8_unicode_ci,
  `number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `pic1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `pic2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `pic3` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `pic4` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `video` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `ladder` int(11) DEFAULT NULL,
  `urgent` int(11) DEFAULT NULL,
  `ladder_urgent` int(11) DEFAULT NULL,
  `continuation` int(11) DEFAULT NULL,
  `ladder_continuation` int(11) DEFAULT NULL,
  `expert` int(11) DEFAULT NULL,
  `parent_category_id` int(11) DEFAULT NULL,
  `delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `slug`, `category_id`, `city_id`, `user_id`, `location_address`, `caption`, `number`, `title`, `price`, `pic1`, `pic2`, `pic3`, `pic4`, `video`, `created_at`, `updated_at`, `deleted_at`, `status`, `ladder`, `urgent`, `ladder_urgent`, `continuation`, `ladder_continuation`, `expert`, `parent_category_id`, `delete`) VALUES
(29, 'alidrere', 'alidrere', 33, 338, 6, '', 'vvvvvvvvvvvvvvvvvv', '15', 'alidrere', 500000, '0', '0', '0', '0', '0', '2018-04-02 14:32:48', '2018-04-02 14:32:48', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0),
(30, 'محصول دستی جدید', 'محصول-دستی-جدید', 33, 173, 6, '', 'محصول دستی جدیدمحصول دستی جدیدمحصول دستی جدید', '15', 'محصول دستی جدید', 500000, '0', '0', '0', '0', '0', '2018-04-02 15:15:58', '2018-04-02 15:15:58', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0),
(31, 'محصول دستی جدیدd d', 'محصول-دستی-جدیدd-d', 33, 179, 6, 'یییی یییی یییی', 'ew ewe  ew', '13', 'محصول دستی جدیدd d', 100000, '0', '0', '0', '0', '0', '2018-04-02 21:34:02', '2018-04-02 21:34:02', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0),
(32, 'محصول خیلی خوب', 'محصول-خیلی-خوب', 33, 338, 6, 'یییی یییی یییی', 'عالیه عالیه عالیه عالیه عالیه عالیه عالیه عالیه', '15', 'محصول خیلی خوب', 100000, 'pic1_1522706712.JPG', 'pic2_1522706712.JPG', 'pic3_1522706712.JPG', 'pic4_1522706712.JPG', '0', '2018-04-02 22:05:13', '2018-04-02 22:05:13', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0),
(35, 'کلاه شاپو', 'کلاه-شاپو', 32, 227, 8, 'یییی یییی یییی', 'ننننننننننننننن    نننننننننننننننننن', '15', 'کلاه شاپو', 100000, 'pic1_1522770366.JPG', '0', 'pic3_1522770366.JPG', '0', '0', '2018-04-03 15:46:06', '2018-04-03 15:46:06', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0),
(36, 'قرش آب', 'قرش-آب', 10, 227, 9, 'یییی یییی یییی', 'تاییر تاییر تاییر تاییر تاییر تاییر تاییر', '155', 'قرش آب', 500000, '0', '0', '0', '0', '0', '2018-04-18 13:40:53', '2018-04-18 13:40:53', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0),
(37, 'مچ بند چرم2', 'مچ-بند-چرم2', 10, 92, 9, 'یییی یییی یییی2', 'سسسس', '12', 'یییی2', 100000, '0', '0', '0', '0', '0', '2018-04-18 14:11:58', '2018-04-18 14:11:58', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0),
(38, 'مچ بند چرم33', 'مچ-بند-چرم33', 10, 55, 9, 'یییی یییی یییی2', 'بببب ثثی بببب ثثی بببب ثثی', '15', 'یییی2', 100000, 'pic1_1524069452.JPG', 'pic2_1524069454.JPG', 'pic3_1524069454.JPG', 'pic4_1524069454.JPG', '0', '2018-04-18 16:37:34', '2018-04-18 16:37:34', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0),
(39, 'مچ بند چرم22', 'مچ-بند-چرم22', 10, 201, 9, 'یییی یییی یییی2', 'جزییات محصول : جزییات محصول : جزییات محصول : جزییات محصول : جزییات محصول :', '12', 'یییی2', 100000, '0', '0', '0', '0', '0', '2018-04-18 16:41:08', '2018-04-18 18:00:27', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0),
(40, 'مچ بند چرم55', 'مچ-بند-چرم55', 10, 415, 9, 'یییی یییی یییی', 'جزییات محصول :جزییات محصول :جزییات محصول :جزییات محصول :جزییات محصول :', '15', 'یییی2', 100000, '0', 'pic2_1524074532.JPG', 'pic3_1524074532.JPG', 'pic4_1524074532.JPG', '0', '2018-04-18 18:02:12', '2018-04-18 18:02:22', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0),
(41, 'مچ بند چرم 44', 'مچ-بند-چرم-44', 30, 361, 10, 'یییی یییی یییی', 'kkkk kkkk kkkk kkkk', '15', 'یییی211', 500000, '0', '0', '0', '0', '0', '2018-04-20 10:40:00', '2018-04-20 10:40:00', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0),
(42, 'مچ بند چرم55m', 'مچ-بند-چرم55m', 30, 338, 10, 'یییی یییی یییی2', 'مچ بند چرم55m مچ بند چرم55m مچ بند چرم55m مچ بند چرم55m', '15', 'یییی211', 100000, 'pic1_1524221289.JPG', '0', '0', '0', '0', '2018-04-20 10:48:10', '2018-04-20 10:48:10', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0),
(43, 'مچ بند چرم55me', 'مچ-بند-چرم55me', 30, 361, 10, 'یییی یییی یییی2', 'مچ بند چرم55m مچ بند چرم55m مچ بند چرم55m مچ بند چرم55m', '15', 'یییی211', 100000, 'pic1_1524249781.JPG', '0', '0', '0', 'video_1524249784.MP4', '2018-04-20 12:02:53', '2018-04-20 18:43:04', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0),
(44, 'll llk', 'll-llk', 30, 415, 2, 'یییی یییی یییی', 'mmkmmk', '15', 'یییی2 ll', 100000, '0', '0', '0', 'pic4_1524255392.JPG', '0', '2018-04-20 20:16:32', '2018-04-20 20:16:39', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1),
(45, 'مچ بند چرم pp', 'مچ-بند-چرم-pp', 43, 1, 2, 'یییی یییی یییی', 'مچ بند چرم pp مچ بند چرم pp مچ بند چرم pp مچ بند چرم pp', '15', 'یییی211', 100000, 'pic1_1524299112.JPG', '0', '0', '0', 'video_1524299144.MP4', '2018-04-21 08:25:12', '2018-04-21 08:25:44', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 5, 0),
(46, 'Casio G-Shock GG-1000-1A3DR Watch For Men', 'Casio-G-Shock-GG-1000-1A3DR-Watch-For-Men', 9, 118, 11, 'جذابیت ساعت‌های اسپرت «جی‌شاک» (G-Shock) از برند مطرح و ژاپنی «کاسیو» (Casio) با توجه به مقاومت سرسختانه در برابر ضربه، طراحی منحصربه‌فرد و قابلیت‌هایی کارآمد و گاها تخصصی بر کسی پوشیده نیست. ساعتی که در ادامه به معرفی آن می‌پردازیم', 'ساعت مچی آنالوگ دیجیتال «کاسیو جی‌شاک» (Casio G-Shock) مدل «GG-1000-1A3DR» از سری ساعت‌های اسپرت و البته حرفه‌ای این برند ژاپنی است که در خانواده‌ی «MUDMASTER» دسته‌بندی می‌شود. همانطور که از عنوان «MUDMASTER» مشخص است، این اطمینان را به شما می‌دهد که در موقعیت‌های مختلف ورزشی، کاری یا هر ماجراجویی دیگری که ممکن است ساعت در معرض تماس با گل و لای و کثیفی باشد، گل و خاک و لجن به داخل ساعت نفوذ نخواهد کرد و هیچ مشکلی برای ساعت ایجاد نمی‌شود. قابلیت مهم دیگر این مدل و دیگر ساعت‌های جی‌شاک گروه «GG-1000» دوسنسوره بودن آن‌ها است. این مدل از دو سنسور دماسنج و قطب‌نمای دیجیتال بهره می‌برد که برای ماجراجویی‌ها یا استفاده‌های ورزشی می‌تواند بسیار کارآمد باشد. ساعت کاسیو جی شاک مدل «GG-1000-1A3DR» از بند رزین و راحتی با رنگ جذاب سبز بهره می‌برد که توجه بسیاری را به خود جلب می‌کند. قاب ساعت هم از جنس رزین است که ویژگی‌هایی نظیر سبکی و همچنین مقاومت قابل‌توجه را در بر دارد. از قابلیت‌های مهم و کاربردی دیگر ساعت می‌توان به کرنوگراف یا زمان‌سنج آن اشاره کرد که قادر است به سنجش زمان با دقت 1/100 ثانیه بپردازد. از جذابیت‌های ظاهری هم می‌توان به طراحی فوق‌العاده زیبای صفحه اشاره کرد که از دو نمایشگر دیجیتال در بالا و پایین بهره می‌برد و یک نشانگر در پایین سمت راست صفحه، به صورت آنالوگ شما را در انتخاب یا سوییچ آسان‌تر بین قابلیت‌های ساعت (modeهای مختلف) یاری می‌کند. مقاومت در برابر شوک و همچنین مقاومت قابل توجه در برابر آب هم از ویژگی‌های مهم کیفی ساعت محسوب می‌شوند. با مقاومت ساعت تا فشار 20 بار یا 200 متر در برابر آب هم هیچ‌گونه نگرانی بابت نفوذ آب به بدنه در شرایط مختلف از جمله شنا وجود خواهد داشت. در مجموع اگر از طرفداران ساعت‌های اسپرت جی‌شاک هستید و این ترکیب رنگ را می‌پسندید به یقین می‌توان گفت این مدل از بهترین گزینه‌ها در این بازه‌ی قیمتی محسوب می‌شود.', '15', 'ساعت مچي عقربه اي مردانه کاسيو جي شاک مدل GG-1000-1A3DR', 100000, 'pic1_1524507749.jpg', 'pic2_1524507750.jpg', 'pic3_1524507750.jpg', 'pic4_1524507750.jpg', 'video_1524507750.MP4', '2018-04-23 18:22:30', '2018-04-23 18:22:30', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE `provinces` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`id`, `name`) VALUES
(1, 'آذربايجان شرقي'),
(2, 'آذربايجان غربي'),
(3, 'اردبيل'),
(4, 'اصفهان'),
(5, 'البرز'),
(6, 'ايلام'),
(7, 'بوشهر'),
(8, 'تهران'),
(9, 'چهارمحال بختياري'),
(10, 'خراسان جنوبي'),
(11, 'خراسان رضوي'),
(12, 'خراسان شمالي'),
(13, 'خوزستان'),
(14, 'زنجان'),
(15, 'سمنان'),
(16, 'سيستان و بلوچستان'),
(17, 'فارس'),
(18, 'قزوين'),
(19, 'قم'),
(20, 'كردستان'),
(21, 'كرمان'),
(22, 'كرمانشاه'),
(23, 'كهكيلويه و بويراحمد'),
(24, 'گلستان'),
(25, 'گيلان'),
(26, 'لرستان'),
(27, 'مازندران'),
(28, 'مركزي'),
(29, 'هرمزگان'),
(30, 'همدان'),
(31, 'يزد');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2018-02-07 05:42:47', '2018-02-07 05:42:47'),
(2, 'user', 'Normal User', '2018-02-07 05:42:47', '2018-02-07 05:42:47');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'ققنوس', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings/February2018/hkmryPSy2K97BRjsznvc.png', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 1, 'pt', 'Post', '2018-02-07 05:42:50', '2018-02-07 05:42:50'),
(2, 'data_types', 'display_name_singular', 2, 'pt', 'Página', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(3, 'data_types', 'display_name_singular', 3, 'pt', 'Utilizador', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(5, 'data_types', 'display_name_singular', 5, 'pt', 'Menu', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(6, 'data_types', 'display_name_singular', 6, 'pt', 'Função', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(7, 'data_types', 'display_name_plural', 1, 'pt', 'Posts', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(8, 'data_types', 'display_name_plural', 2, 'pt', 'Páginas', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(9, 'data_types', 'display_name_plural', 3, 'pt', 'Utilizadores', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(11, 'data_types', 'display_name_plural', 5, 'pt', 'Menus', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(12, 'data_types', 'display_name_plural', 6, 'pt', 'Funções', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(22, 'menu_items', 'title', 3, 'pt', 'Publicações', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(23, 'menu_items', 'title', 4, 'pt', 'Utilizadores', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(24, 'menu_items', 'title', 5, 'pt', 'Categorias', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(25, 'menu_items', 'title', 6, 'pt', 'Páginas', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(26, 'menu_items', 'title', 7, 'pt', 'Funções', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(27, 'menu_items', 'title', 8, 'pt', 'Ferramentas', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(28, 'menu_items', 'title', 9, 'pt', 'Menus', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(29, 'menu_items', 'title', 10, 'pt', 'Base de dados', '2018-02-07 05:42:51', '2018-02-07 05:42:51'),
(30, 'menu_items', 'title', 12, 'pt', 'Configurações', '2018-02-07 05:42:51', '2018-02-07 05:42:51');

-- --------------------------------------------------------

--
-- Table structure for table `usages`
--

CREATE TABLE `usages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `family_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telegram_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `linkdin_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `twitter_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `background_banner` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `custom_design_and_production` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `teaching_and_training` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `e_learning` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `store_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `art_degree` int(11) NOT NULL DEFAULT '0',
  `pic_certificate` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `teach_learning_plan` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `phone_number`, `family_name`, `site_address`, `telegram_id`, `email`, `instagram_id`, `linkdin_id`, `twitter_id`, `background_banner`, `custom_design_and_production`, `teaching_and_training`, `e_learning`, `store_address`, `avatar`, `art_degree`, `pic_certificate`, `password`, `remember_token`, `created_at`, `updated_at`, `teach_learning_plan`, `status`) VALUES
(1, 1, 'Admin', '09120975635', NULL, NULL, NULL, 'admin@admin.com', NULL, NULL, NULL, NULL, NULL, '0', '0', NULL, 'users/February2018/Rm00v2Hb6qBjNreqoqFj.jpg', 0, NULL, '$2y$10$HLe3Bd8gtL9snCi2T9LXPeLKYW3Tmc9OqstRZO7LbrsxwCwfayqky', 'IvUIGiFEYl2fYTLfZoJakm5kYAPsaOAvh8XtKWFveFJ00WKIHaSV00E30W0P', '2018-02-07 05:42:50', '2018-02-07 06:06:50', 0, 0),
(2, NULL, 'پزشک', '09120975636', NULL, NULL, NULL, 'h.mosaferkocholo@gmail.com', NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0, NULL, '$2y$10$FtcpdI/Gx1MTTapxZIK.0OiY0vlT92SCMlD8Kcvy/JzTvW4gFSYCO', 'f1O54MEA51YtyIQyZpAPS1pvmqTAykJ1P5Y62rwEWSp5vr6tbGwiphOrG6TM', '2018-04-02 07:56:32', '2018-04-02 07:56:32', 0, 0),
(3, NULL, 'آذربايجان شرقي', '09120975634', NULL, NULL, NULL, 'afmin@admin.com', NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0, NULL, '$2y$10$gacRzB/N7ONxRGVrqWEKJ.ZALdYA5ht7t270qtuADrLP5/825.5Ge', 'm5XD7kSHLjpBN8HHasgtVYUkl4fKv1vlE80gtELxzCxSdCeFoXFGXs7YWJam', '2018-04-02 08:06:32', '2018-04-02 08:06:32', 0, 0),
(4, NULL, 'پزشک', '09120975632', NULL, NULL, NULL, 'saruman.valar@gmail.com', NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0, NULL, '$2y$10$MeJHs4pzWTIIvs6SVBI40e4w1ZS8p2Zu6HNktmyDUQQ7KaECe6oj2', 'aDg172rCDET3fAesJgsjTGY4IuhhPZ2wvs8kiJCtcT7UJ2cL1PrDs9Vscf0r', '2018-04-02 10:33:56', '2018-04-02 10:33:56', 0, 0),
(5, 2, '123456', '09194331008', 'nasrollahy', 'xisco.ir', 'hamidhmz@', 'h.mosaferkocholo@gmail.comddds', NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0, NULL, '111222', NULL, '2018-04-02 10:39:47', '2018-04-02 10:39:47', 0, 0),
(6, 2, 'harchi', '09194331008', 'nasrollahy', 'xisco.ir', 'hamidhmz@', 'afmin@admin.comc', NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0, NULL, '123456', NULL, '2018-04-02 14:32:48', '2018-04-02 14:32:48', 0, 0),
(8, 2, 'esjeee', '09194331008', 'nasrollahy', 'xisco.ir', 'hamidhmz@', 'afmin@admin.comm', NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0, NULL, '123456', NULL, '2018-04-03 15:46:06', '2018-04-03 15:46:06', 0, 0),
(9, 2, 'esmekhodeshe', '091943310083', 'nasrollahy', 'xisco.ir', 'hamidhmz@', 'admin@admin.comff', NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0, NULL, '123456', NULL, '2018-04-18 13:40:52', '2018-04-18 13:40:52', 0, 0),
(10, 2, 'harchi dari', '09194331008', 'nasrollahy', 'xisco.ir', 'hamidhmz@', 'h.mosaferkocholo@gmail.com44', NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0, NULL, '123456', NULL, '2018-04-20 10:40:00', '2018-04-20 10:40:00', 0, 0),
(11, 2, 'دیجی کالا', '09120975633', 'محسنی', 'digikala.com', 'hamidhmz@', 'nasrollahy.hamidreza@gmail.com', NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0, NULL, '$2y$10$GxppszbPuivutZEXPdlHb.ZvDWaffqz6ypwDoA44d0O0JK/zHuQqm', NULL, '2018-04-23 18:22:28', '2018-04-23 18:22:28', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clips`
--
ALTER TABLE `clips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_user_foreign_key` (`user_id`),
  ADD KEY `comments_product_id_foreign_key` (`product_id`);

--
-- Indexes for table `curriculums`
--
ALTER TABLE `curriculums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `materials`
--
ALTER TABLE `materials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `parent_categories`
--
ALTER TABLE `parent_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_groups`
--
ALTER TABLE `permission_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permission_groups_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_user_id_index` (`user_id`);

--
-- Indexes for table `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `usages`
--
ALTER TABLE `usages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=454;

--
-- AUTO_INCREMENT for table `clips`
--
ALTER TABLE `clips`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `curriculums`
--
ALTER TABLE `curriculums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `materials`
--
ALTER TABLE `materials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `parent_categories`
--
ALTER TABLE `parent_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `permission_groups`
--
ALTER TABLE `permission_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `provinces`
--
ALTER TABLE `provinces`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `usages`
--
ALTER TABLE `usages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_product_id_foreign_key` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_user_id_user_foreign_key` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_user_id_index` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
