<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');

Route::any('/search/{city}/brows/{slug?}', 'SearchController@showResult');

//Route::any('/ajax/search','AjaxController@passAjaxRequest');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/shop/{slug}', 'ShopController@showShopToOthers');

Route::get('/auth', 'VerificationController@specifyNumberPhone');

Route::get('/details/{id}/{slug}', 'ProductController@showDetails');

Route::get('/store/preview/{slug}', 'ProductController@preview');

Route::get('/new/category/first', 'ProductController@storeFirstCategoryShow');

Route::get('/new/category/second/{parent_category}', 'ProductController@storeSecondCategoryShow');

Route::get('/new/product/{parent_category}/{category}', 'ProductController@storeShow');

Route::get('/delete/product-picture/pic1/{id}', 'ProductController@deletePic1');

Route::get('/delete/product-picture/pic2/{id}', 'ProductController@deletePic2');

Route::get('/delete/product-picture/pic3/{id}', 'ProductController@deletePic3');

Route::get('/delete/product-picture/pic4/{id}', 'ProductController@deletePic4');

Route::get('/delete/product-video/{id}', 'ProductController@deleteVideo');

Route::get('/delete/product/{id}', 'ProductController@deleteProduct');

Route::get('/login', 'LoginController@previewLoginForm');

Route::post('/login', 'Auth\LoginController@login');

Route::post('/edit/product/details/{id}', 'ProductController@editDetails');

Route::post('/create/comment/{id}', 'CommentController@createNewComment');

Route::post('/create/user-and-product', 'ProductController@createNewProduct');

Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
