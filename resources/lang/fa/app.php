<?php

return [

    'appname' => 'سامانه آموزش',
    'toman' => 'تومان',
    'price' => 'قیمت (تومان)',
    'filter' => 'فیلتر کردن',
    'search' => 'جستجو',
    'products' => 'محصولات',
];
