<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{setting('site.title')}}</title>

    <link rel="stylesheet" type="text/css" href="{{asset('css/site/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/site/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/site/app.min.css')}}">
    @yield('css')
</head>

<body class="">
<nav class="mynav navbar navbar-default bg-white container-fluid" role="navigation">

    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="fa fa-bars fa-2x"></span>

        </button>
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{asset('/storage').'/'.setting('site.logo')}}" alt="logo">
        </a>
        <div class="city-btn">
            <button class="btn " data-toggle="modal" data-target="#myModal">انتخاب شهر
                <i class="fa fa-angle-down"></i>
            </button>
        </div>
    </div>

    <div class="collapse navbar-collapse" id="example-navbar-collapse">

        <div class="pull-left">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('/') }}">ققنوس من</a>
                </li>
                {{--<li>--}}
                {{--<a href="#">چت</a>--}}
                {{--</li>--}}
                <li>
                    <a href="{{ url('/') }}">درباره ما </a>
                </li>
                <li>
                    <a href="{{ url('/') }}">بلاگ</a>
                </li>
                <li>
                    <a href="{{ url('/') }}">شرایط راهنما</a>
                </li>
                <li>
                    <a href="{{ url('/') }}">تماس با ما</a>
                </li>


            </ul>
            <div class="send-add pc ">
                <a href="{{url('/new/category/first')}}" class="">
                    <button class="btn danger-light">ارسال رایگان آگهی</button>
                </a>
            </div>
            @if(auth()->check())
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
                <div class="send-add pc ">
                    <a class="btn danger-light"
                       onclick="event.preventDefault();      document.getElementById('logout-form').submit();">
                        خروج </a>
                </div>
            @else
                <div class="send-add pc ">
                    <a href="{{ url('/login') }}" class="btn danger-light"> ورود </a>
                </div>
            @endif
        </div>
    </div>
    <div class="send-add min">
        <a href="#" class="">
            <button class="btn danger-light">ارسال رایگان آگهی</button>
        </a>
    </div>

</nav>

{{--start main--}}
@yield('content')
{{--end main--}}
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Modal -->
<div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>

                <h4 class="modal-title" id="myModalLabel">
                    جستجوی سریع ...
                </h4>
            </div>

            <div class="modal-body row">
                <div class="home-link margin-top-5 padding-5">
                    <div role="form">
                        <div class="form-group">
                            <input id="filter2" class="form-control" type="search"
                                   placeholder="جستجوی سریع نام شهر ..."/>
                        </div>
                        <ul id="list2" class="lists">
                            @foreach($cities as $city)
                                <li>
                                    <a class="btn items " title="{{ $city->name }}"
                                       href="{{ url('/search').'/'.str_replace(' ','-',$city->name).'/brows' }}">
                                        <span>{{ str_limit($city->name,10) }}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="modal-footer row">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    بستن
                </button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->

</div>
<!-- /.modal -->

<script>
    @if (session()->has('done'))
    alert('{{ session()->get("done") }}');
    @endif
    @if (session()->get('fail'))
    alert('{{ session()->get("fail") }}');
    @endif
</script>
<script type="text/javascript" src="{{asset('js/site/jquery-3.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/site/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/site/scripts.js')}}"></script>
@yield('js')
</body>

</html>