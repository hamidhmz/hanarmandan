<?php
// config
$link_limit = 7; // maximum number of links (a little bit inaccurate, but will be ok for now)
$ff = [];
$ss = null;

if (!empty($_GET)) {

    foreach ($_GET as $key => $value) {
        if ($key == 'page') continue;
        $ss .= '&' . $key . '=' . $value;
        array_push($ff, [$key => $value]);
    }
}
// write this is before your return view
// this code make your softer paginate
// if (isset($_GET['page']) && $_GET['page'] != '') {
//     if ($_GET['page'] > $products->lastPage() ) return redirect(url()->current());
// }
$r = 0;
$t = 0;
?>

@if ($paginator->lastPage() > 1)
    <ul class="pagination">
        <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
            @if(null !== $ss)
                <a href="{{ $paginator->previousPageUrl().$ss }}">«</a>
            @else
                <a href="{{ $paginator->previousPageUrl() }}">«</a>
            @endif
        </li>
        @if($paginator->currentPage() != 1 && $paginator->currentPage() != 2 && $paginator->currentPage() != 3)
            <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
                @if(null !== $ss)
                    <a href="{{ $paginator->url(1).$ss }}">{{ 1 }}</a>
                @else
                    <a href="{{ $paginator->url(1) }}">{{ 1 }}</a>
                @endif
            </li>
        @endif
        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            <?php
            $half_total_links = floor($link_limit / 2);
            $from = $paginator->currentPage() - $half_total_links;
            $to = $paginator->currentPage() + $half_total_links;
            if ($paginator->currentPage() < $half_total_links) {
                $to += $half_total_links - $paginator->currentPage();
            }
            if ($paginator->lastPage() - $paginator->currentPage() < $half_total_links) {
                $from -= $half_total_links - ($paginator->lastPage() - $paginator->currentPage()) - 1;
            }

            ?>
            @if ($from < $i && $i < $to)
                @php
                    if ($i == 1) $r = 1;
                @endphp
                @if($r != 1)
                    <li class="">
                        @if(null !== $ss)
                            <a href="{{ $paginator->url($i-3).$ss }}">...</a>
                        @else
                            <a href="{{ $paginator->url($i-3) }}">...</a>
                        @endif
                    </li>
                    @php
                        $r = 1;
                    @endphp
                @endif
                <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                    @if(null !== $ss)
                        <a href="{{ $paginator->url($i).$ss }}">{{ $i }}</a>
                    @else
                        <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
                    @endif
                    @php
                        $l = $i;
                    @endphp
                </li>
            @endif
        @endfor
        @php
            if ($l == $paginator->lastPage()) $t = 1;
        @endphp
        @if($t != 1)
            <li class="">
                @if(null !== $ss)
                    <a href="{{ $paginator->url($l + 3).$ss }}">...</a>
                @else
                    <a href="{{ $paginator->url($l + 3) }}">...</a>
                @endif
            </li>
            @php
                $t = 1;
            @endphp
        @endif
        @if($l != $paginator->lastPage())
            <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
                @if(null !== $ss)
                    <a href="{{ $paginator->url($paginator->lastPage()).$ss }}">{{ $paginator->lastPage() }}</a>
                @else
                    <a href="{{ $paginator->url($paginator->lastPage()) }}">{{ $paginator->lastPage() }}</a>
                @endif
            </li>
        @endif
        <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
            @if(null !== $ss)
                <a href="{{ $paginator->nextPageUrl().$ss }}">»</a>
            @else
                <a href="{{ $paginator->nextPageUrl() }}">»</a>
            @endif
        </li>
    </ul>
@endif