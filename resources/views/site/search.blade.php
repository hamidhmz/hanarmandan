@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/site/vendor/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/site/vendor/bootstrap-slider.css')}}">
@endsection
@section('js')
    <script type="text/javascript" src="{{asset('js/site/bootstrap-slider.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/site/select2.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('.collapse').collapse('show')
            });

            // $(".js-example-basic-multiple").select2({
            //     placeholder: "براساس گروه هنری ",
            //     allowClear: true
            // });
            //
            // $(".js-example-basic-multiple2").select2({
            //     placeholder: "انتخاب هنرمند ..  ",
            //     allowClear: true
            // });
            $("#ex2").change(function () {
                var lowerf = $('#ex2').val().split(",")[0];
                var upperf = $("#ex2").val().split(',')[1];
                $("#lower").val(lowerf);
                $("#upper").val(upperf);
            })
        });
        // $("#ex2").slider({});

        // Without JQuery
        var slider = new Slider('#ex2', {});
    </script>
@endsection
@section('content')
    @php
        $ffget = '';
        $i = 0;
        if (!empty($_GET)) {

            foreach ($_GET as $key => $value) {
                if ($key == 'page') continue;
                $ffget .= '&' . $key . '=' . $value;
            }
        }
    @endphp
    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-3 ">
                    <div class="search-right margin-top-50">
                        <h3 class="font-sm-2 teal">بخش محصولات صنایع دستی</h3>
                        <div class="panel-group" id="accordion">
                            @foreach($parent_categories as $parent_category)
                                @php
                                    $i++;
                                @endphp
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title font-sm">
                                            <a class="accordion-toggle" data-toggle="collapse"
                                               data-parent="#accordion{{ $i }}"
                                               href="#collapse{{ $i }}">
                                                {{ $parent_category->name }}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse{{ $i }}" class="panel-collapse collapse ">
                                        <div class="panel-body">
                                            <ul>
                                                <li>
                                                    <a href="{{ url('/search').'/'.$city.'/brows/'.$parent_category->slug  }}">{{ 'همه' }}</a>
                                                </li>
                                                @foreach($parent_category->categories()->get() as $child)
                                                    <li>
                                                        <a href="{{ url('/search').'/'.$city.'/brows/'.$child->slug }}">{{ $child->name }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        {{--<div class="honary-gp">--}}
                        {{--<h3 class="font-sm-2 teal">بخش گروه هنری :</h3>--}}
                        {{--<div class="">--}}
                        {{--<select class="js-example-basic-multiple" name="states[]" >--}}
                        {{--<option value="AL">مقدار</option>--}}
                        {{--<option value="WY">مقدار 2</option>--}}
                        {{--<option value="WY">مقدار 3</option>--}}
                        {{--<option value="WY">مقدار 4</option>--}}
                        {{--<option value="WY">مقدار 5</option>--}}
                        {{--</select>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="honary-gp">--}}
                        {{--<h3 class="font-sm-2 teal">بخش هنرمند :</h3>--}}
                        {{--<div class="">--}}
                        {{--<select class="js-example-basic-multiple2" name="states[]" >--}}
                        {{--<option value="AL">مقدار</option>--}}
                        {{--<option value="WY">مقدار 2</option>--}}
                        {{--<option value="WY">مقدار 3</option>--}}
                        {{--<option value="WY">مقدار 4</option>--}}
                        {{--<option value="WY">مقدار 5</option>--}}
                        {{--</select>--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        <div class="honary-gp">
                            <h3 class="font-sm-2 teal">@lang('app.price')</h3>
                            <div class="">
                                <b class="pull-right">{{ number_format($lower_prices->price).' '.__('app.toman') }}</b>
                                <input id="ex2" type="text" class="span2" value=""
                                       data-slider-min="{{ $lower_prices->price }}"
                                       data-slider-max="{{ $heigh_prices->price }}" data-slider-step="50"
                                       data-slider-value="[{{ $lower_prices->price }},{{ $heigh_prices->price }}]"/>
                                <b class="pull-left">{{ number_format($heigh_prices->price).' '.__('app.toman') }}</b>
                            </div>
                        </div>
                        <form id="form1" method="get">
                            <input type="hidden" id="lower" name="lower">
                            <input type="hidden" id="upper" name="upper">
                            <div class="text-center margin-top-5">
                                <button type="submit" class="btn btn-base info-dark" form="form1"
                                        id="hidden">@lang('app.filter')</button>
                            </div>
                        </form>
                        {{--<div class="honary-gp">--}}
                        {{--<h3 class="font-sm-2 teal">بخش کاربرد :</h3>--}}
                        {{--<div class="metod">--}}
                        {{--<ul>--}}
                        {{--<li>--}}
                        {{--<input type="checkbox" name="" id="c1">--}}
                        {{--<label for="c1">مقدار 1</label>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                        {{--<input type="checkbox" name="" id="c2">--}}
                        {{--<label for="c2">مقدار 2</label>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                        {{--<input type="checkbox" name="" id="c3">--}}
                        {{--<label for="c3">مقدار 3</label>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                        {{--<input type="checkbox" name="" id="c4">--}}
                        {{--<label for="c4">مقدار 4</label>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                        {{--<input type="checkbox" name="" id="c5">--}}
                        {{--<label for="c5">مقدار 5</label>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                        {{--<input type="checkbox" name="" id="c6">--}}
                        {{--<label for="c6">مقدار 6</label>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                        {{--<input type="checkbox" name="" id="c7">--}}
                        {{--<label for="c7">مقدار 7</label>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                        {{--<input type="checkbox" name="" id="c8">--}}
                        {{--<label for="c8">مقدار 8</label>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                        {{--<input type="checkbox" name="" id="c9">--}}
                        {{--<label for="c9">مقدار 9</label>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <div class="honary-gp">
                            <h3 class="font-sm-2 teal">بخش برخی از امکانات ما :</h3>
                            <div class="font-sm">
                                <ul>
                                    <li>
                                        <strong class="dis-blok bg-red font-xs white text-center padding-tb-10">
                                            <i class="fa fa-bullhorn"></i> افتتاح فروشگاه اختصاصی برای هر هنرمند
                                        </strong>
                                    </li>
                                    <li>
                                        <strong class="dis-blok bg-teal font-xs white text-center padding-tb-10">
                                            <i class="fa fa-shopping-cart"></i> خرید و فروش محصولات متنوع هنری </strong>
                                    </li>
                                    <li>
                                        <strong class="dis-blok bg-orange font-xs white text-center padding-tb-10">
                                            <i class="fa fa-paint-brush"></i> خرید و فروش ملزومات و مواد اولیه </strong>
                                    </li>
                                    <li>
                                        <strong class="dis-blok bg-fuchsia font-xs white text-center padding-tb-10">
                                            <i class="fa fa-leanpub"></i> برگزاری دوره های آموزشی هنری </strong>
                                    </li>
                                    <li>
                                        <strong class="dis-blok bg-navy font-xs white text-center padding-tb-10">
                                            <i class="fa fa-puzzle-piece"></i> طراحی و تولید سفارشی محصولات </strong>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8 col-md-9">
                    <div class="products margin-top-50">
                        <div class="row">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h2>@lang('app.search') @lang('app.products') </h2>
                                </div>
                                <div class="panel-body">
                                    <div class="row">

                                        <div class="col-md-6">
                                            <form class="input-group">
                                                <div class="col-xs-8">
                                                    <input type="text" name="width_search"
                                                           class="form-control pull-right"
                                                           placeholder="{{ __('app.search') }}">
                                                </div>
                                                <div class="col-xs-4">
                                                    <button type="" class="btn btn-base success-light pull-right">
                                                        @lang('app.search')
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-xs-6">
                                                <select class="form-control" name="forma"
                                                        onchange="location = this.value;">
                                                    <option disabled="disabled" selected="selected"> قیمت محصول</option>
                                                    @php
                                                        $productss = \App\Product::groupby('price')->get();
                                                    @endphp
                                                    @foreach($productss  as $staytype)

                                                        <option value="{{url()->current().'?'.$ffget.'&price='.$staytype->price}}">{{ number_format($staytype->price) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-xs-6">
                                                <select class="form-control" onchange="location = this.value;">
                                                    <option disabled="disabled" selected="selected"> انتخاب هنرمند
                                                    </option>
                                                    @php
                                                        $userss = \App\Product::groupby('user_id')->get();
                                                    @endphp
                                                    @foreach($userss  as $staytype)

                                                        <option value="{{url()->current().'?'.$ffget.'&producer='.$staytype->user_id}}">{{ $staytype->user()->first()->name }}</option>

                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="clearfix margin-top-30"></div>

                                    <div class="row">
                                        @foreach($products as $product)
                                            <div class="col-xs-6 col-sm-4 col-md-3">
                                                <div class="products-item">
                                                    <div class="item-img">
                                                        @php
                                                            if ($product->pic1 != '0'){
                                                                $picture = asset('storage/img/users/userid_'.$product->user_id.'/thumbnail/'.$product->pic1);
                                                            }elseif($product->pic2 != '0'){
                                                                $picture = asset('storage/img/users/userid_'.$product->user_id.'/thumbnail/'.$product->pic2);
                                                            }elseif($product->pic3 != '0'){
                                                                $picture = asset('storage/img/users/userid_'.$product->user_id.'/thumbnail/'.$product->pic3);
                                                            }elseif($product->pic4 != '0'){
                                                                $picture = asset('storage/img/users/userid_'.$product->user_id.'/thumbnail/'.$product->pic4);
                                                            }else{
                                                                $picture = asset('storage/img/products/1.jpg');
                                                            }
                                                        @endphp
                                                        <a href="{{ url('/details/'.$product->id.'/'.$product->slug) }}"><img src="{{ $picture }}" alt=""
                                                                        class="img-responsive"></a>
                                                    </div>
                                                    <div class="text-center">
                                                        <h4 class="font-sm-2">{{ $product->title }}</h4>
                                                        <strong class="green font-sm">{{ number_format($product->price) .' '.__('app.toman') }} تومان</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="margin-top-50 margint-bottom-50">
                        <div class="center-box">
                            {{ $products->links('layouts.paginate') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection