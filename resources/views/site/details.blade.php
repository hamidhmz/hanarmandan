@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/site/vendor/slick.css')}}">
@endsection
@section('js')
    <script type="text/javascript" src="{{asset('js/site/slick.min.js')}}"></script>
    <script type="text/javascript">

        // $('.slickSlider').slick({
        //   dots: false,
        //   infinite: false,
        //   speed: 300,
        //   slidesToShow: 1,
        //   centerMode: true,
        //   variableWidth: true
        // });
        $('.slickSlider').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });


        $(document).ready(function () {
            $(function () {
                $('.collapse').collapse('hide')
            });


        });
        // $("#ex2").slider({});
        $('.carousel').carousel({
            interval: 4000
        });

    </script>
@endsection
@section('content')
    @php
        $ffget = '';
        $i = 0;
        if (!empty($_GET)) {

            foreach ($_GET as $key => $value) {
                if ($key == 'page') continue;
                $ffget .= '&' . $key . '=' . $value;
            }
        }
        if (\Cookie::get('city') !== null){
            $city = \Cookie::get('city');
        }else{
            $city = 'تهران';
        }
        $counter = 0;
    @endphp
    <div class="main">
        <div class="min-container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-right margin-top-50">
                        <ol class="breadcrumb">
                            <li><a href="{{ url('/')  }}"><i class="fa fa-home"></i> ققنوس </a></li>
                            <li>
                                <a href="{{ url('/search').'/'.$city.'/brows/'.$product->parent_category()->first()->slug  }}">{{ $product->parent_category()->first()->name }}</a>
                            </li>
                            <li>
                                <a href="{{ url('/search').'/'.$city.'/brows/'.$product->category()->first()->slug }}">{{ $product->category()->first()->name }}</a>
                            </li>
                            <li class="active">{{ $product->title }}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4 col-md-3 ">
                    <div class="search-right margin-top-10">
                        <h3 class="font-sm-2 teal">بخش محصولات صنایع دستی</h3>
                        <div class="panel-group" id="accordion">
                            @foreach($parent_categories as $parent_category)
                                @php
                                    $i++;
                                @endphp
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title font-sm">
                                            <a class="accordion-toggle" data-toggle="collapse"
                                               data-parent="#accordion{{ $i }}"
                                               href="#collapse{{ $i }}">
                                                {{ $parent_category->name }}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse{{ $i }}" class="panel-collapse collapse ">
                                        <div class="panel-body">
                                            <ul>
                                                <li>
                                                    <a href="{{ url('/search').'/'.$city.'/brows/'.$parent_category->slug  }}">{{ 'همه' }}</a>
                                                </li>
                                                @foreach($parent_category->categories()->get() as $child)
                                                    <li>
                                                        <a href="{{ url('/search').'/'.$city.'/brows/'.$child->slug }}">{{ $child->name }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="honary-gp">
                            <h3 class="font-sm-2 teal">بخش برخی از امکانات ما :</h3>
                            <div class="font-sm">
                                <ul>
                                    <li>
                                        <strong class="dis-blok bg-red font-xs white text-center padding-tb-10"><i
                                                    class="fa fa-bullhorn"></i> افتتاح فروشگاه اختصاصی برای هر هنرمند
                                        </strong>
                                    </li>
                                    <li>
                                        <strong class="dis-blok bg-teal font-xs white text-center padding-tb-10"><i
                                                    class="fa fa-shopping-cart"></i> خرید و فروش محصولات متنوع هنری
                                        </strong>
                                    </li>
                                    <li>
                                        <strong class="dis-blok bg-orange font-xs white text-center padding-tb-10"><i
                                                    class="fa fa-paint-brush"></i> خرید و فروش ملزومات و مواد اولیه
                                        </strong>
                                    </li>
                                    <li>
                                        <strong class="dis-blok bg-fuchsia font-xs white text-center padding-tb-10"><i
                                                    class="fa fa-leanpub"></i> برگزاری دوره های آموزشی هنری </strong>
                                    </li>
                                    <li>
                                        <strong class="dis-blok bg-navy font-xs white text-center padding-tb-10"><i
                                                    class="fa fa-puzzle-piece"></i> طراحی و تولید سفارشی محصولات
                                        </strong>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8 col-md-9">
                    <div class="panel margin-top-10">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6 ">
                                    <div class="product-detail">
                                        <h1 class="font-md font-600 teal dis-blok">{{ $product->title }} </h1>
                                        <h5 class="   dis-blok">{{ $product->name }} </h5>
                                        <div class="specs margin-top-30">
                                            <ul>
                                                <li class="margin-top-10"><strong>نام هنرمند :</strong>&nbsp; &nbsp;<small>
                                                        {{ $product->user()->first()->name }}
                                                    </small>
                                                </li>
                                                <li class="margin-top-10"><strong>توضیحات :</strong>&nbsp; &nbsp;<small>
                                                        {{ $product->caption }}
                                                    </small>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="price margin-top-50">
                                            <strong>قیمت محصول :</strong>&nbsp; &nbsp;
                                            <span class="font-sm-2 green">{{ number_format($product->price) }}
                                                تومان</span>
                                        </div>
                                        <div class="margin-top-30 text-center">
                                            <a href="" class="btn btn-base green-dark"><i
                                                        class="fa fa-shopping-cart"></i> سفارش دهید</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 ">
                                    <div class="perview-img ">
                                        <div id="myCarousel" class="carousel slide">
                                            <!-- Carousel indicators -->
                                            <ol class="carousel-indicators">
                                                @php
                                                    $counter = 0;
                                                @endphp
                                                @if(($product->pic1) != '0')
                                                    <li data-target="#myCarousel" data-slide-to="{{ $counter }}"
                                                        @if($counter == 0)class="active"@endif></li>
                                                    @php
                                                        $counter++;
                                                    @endphp
                                                @endif
                                                @if(($product->pic2) != '0')
                                                    <li data-target="#myCarousel" data-slide-to="{{ $counter }}"
                                                        @if($counter == 0)class="active"@endif></li>
                                                    @php
                                                        $counter++;
                                                    @endphp
                                                @endif
                                                @if(($product->pic3) != '0')
                                                    <li data-target="#myCarousel" data-slide-to="{{ $counter }}"
                                                        @if($counter == 0)class="active"@endif></li>
                                                    @php
                                                        $counter++;
                                                    @endphp
                                                @endif
                                                @if(($product->pic4) != '0')
                                                    <li data-target="#myCarousel" data-slide-to="{{ $counter }}"
                                                        @if($counter == 0)class="active"@endif></li>
                                                    @php
                                                        $counter++;
                                                    @endphp
                                                @endif
                                            </ol>
                                            <!-- Carousel items -->
                                            <div class="carousel-inner">
                                                @php
                                                    $counter = 0;
                                                    $active = '';
                                                @endphp
                                                @if(($product->pic1) != '0')
                                                    @php
                                                        if($counter == 0) $active = 'active';
                                                    @endphp
                                                    <div class="item {{ $active }}">
                                                        <img src="{{asset('storage/img/users/userid_'.$product->user_id.'/'.$product->pic1)}}"
                                                             alt="{{ $counter }} slide">
                                                    </div>
                                                    @php
                                                        $active = '';
                                                        $counter++;
                                                    @endphp
                                                @endif
                                                @if(($product->pic2) != '0')
                                                    @php
                                                        if($counter == 0) $active = 'active';
                                                    @endphp
                                                    <div class="item {{ $active }}">
                                                        <img src="{{asset('storage/img/users/userid_'.$product->user_id.'/'.$product->pic2)}}"
                                                             alt="{{ $counter }} slide">
                                                    </div>
                                                    @php
                                                        $active = '';
                                                        $counter++;
                                                    @endphp
                                                @endif
                                                @if(($product->pic3) != '0')
                                                    @php
                                                        if($counter == 0) $active = 'active';
                                                    @endphp
                                                    <div class="item {{ $active }}">
                                                        <img src="{{asset('storage/img/users/userid_'.$product->user_id.'/'.$product->pic3)}}"
                                                             alt="{{ $counter }} slide">
                                                    </div>
                                                    @php
                                                        $active = '';
                                                        $counter++;
                                                    @endphp
                                                @endif
                                                @if(($product->pic4) != '0')
                                                    @php
                                                        if($counter == 0) $active = 'active';
                                                    @endphp
                                                    <div class="item {{ $active }}">
                                                        <img src="{{asset('storage/img/users/userid_'.$product->user_id.'/'.$product->pic4)}}"
                                                             alt="{{ $counter }} slide">
                                                    </div>
                                                    @php
                                                        $active = '';
                                                        $counter++;
                                                    @endphp
                                                @endif
                                            </div>
                                            <!-- Carousel nav -->
                                            <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                                            <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- //tabs -->
                    <div class="row">
                        <div class="col-xs-12">
                            <ul id="myTab" class="nav nav-tabs">
                                <li class="active"><a class="font-md" href="#description" data-toggle="tab">توضیحات و
                                        مشخصات</a></li>
                                <li><a class="font-md" href="#comments" data-toggle="tab">نظرات</a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content">
                                <!-- Start description codes  -->
                                <div class="tab-pane panel padding-top-30 padding-bottom-30 fade in active"
                                     id="description">
                                    <div class="specs">
                                        <ul>
                                            <li class="margin-top-10"><strong>نام محصول :</strong>&nbsp; &nbsp;<small>
                                                    {{ $product->title }}
                                                </small>
                                            </li>
                                            <li class="margin-top-10"><strong>اسم فروشنده :</strong>&nbsp; &nbsp;<small>
                                                    {{ $product->user()->first()->name.'-'.$product->user()->first()->family_name }}
                                                </small>
                                            </li>
                                            <li class="margin-top-10"><strong>تعداد باقیمانده :</strong>&nbsp; &nbsp;<small>
                                                    {{ $product->number }}
                                                </small>
                                            </li>
                                            <li class="margin-top-10"><strong>آدرس فروشگاه :</strong>&nbsp; &nbsp;<small>
                                                    <a href="{{ url('/shop/'.str_replace(' ' ,'-' ,$product->user()->first()->name)) }}">{{ $product->user()->first()->name }}</a>
                                                </small>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- End perview codes  -->
                                <!-- Start commentse codes  -->
                                <div class="tab-pane panel padding-5 padding-top-30 padding-bottom-30  fade"
                                     id="comments">
                                    <!--Comments Area-->
                                    <div class="padding-5 margin-top-10 margin-bottom-15">
                                        @if( !auth()->check())
                                            <p>جهت ثبت نظر، باید وارد سایت شوید. </p>
                                        @endif
                                        @if($product->comments()->get()->count() == 0)
                                            <p>برای این محصول نظری ثبت نشده است.</p>
                                        @endif
                                    </div>
                                    <div class="comments-area">
                                        @if($product->comments()->get()->count() != 0)
                                            <div class="group-title"><h2>نظرات
                                                    ( {{ $product->comments()->get()->count() }} )</h2></div>
                                        @endif

                                    <!--Comment Box-->
                                        @foreach($product->comments()->where('comment_id', null)->get() as $comment)
                                            @php
                                                if ($comment->user()->first()->avatar == '0'){
                                                    $avatar = 'storage/img/avatar/1.png';
                                                }else{
                                                    $avatar = 'storage/img/users/userid_'.$comment->user_id.'/'.$comment->user()->first()->avatar;
                                                }
                                                $createTime = new \Verta($comment->created_at);
                                            @endphp
                                            <div class="comment-box">
                                                <div class="comment">
                                                    <div class="author-thumb">
                                                        <img src="{{ asset($avatar) }}"
                                                             alt="{{ $comment->user()->first()->name }}">
                                                    </div>
                                                    <div class="comment-inner">
                                                        <div class="comment-info clearfix">
                                                            <strong>{{ $comment->user()->first()->name }}</strong>
                                                            <div class="comment-time">{{ $createTime }}</div>
                                                        </div>
                                                        <div class="text">
                                                            {{ $comment->context }}
                                                        </div>

                                                    </div>
                                                </div>
                                                @foreach($product->comments()->where('comment_id', $comment->id)->get() as $comment1)
                                                    @php
                                                        if ($comment1->user()->first()->avatar == '0'){
                                                            $avatar1 = 'storage/img/avatar/1.png';
                                                        }else{
                                                            $avatar1 = 'storage/img/users/userid_'.$comment1->user_id.'/'.$comment1->user()->first()->avatar;
                                                        }
                                                        $createTime1 = new \Verta($comment1->created_at);
                                                    @endphp
                                                    <div class="comment-reply-box">
                                                        <div class="comment">
                                                            <div class="author-thumb">
                                                                <img src="{{ asset($avatar1) }}"
                                                                     alt="user avatar">
                                                            </div>
                                                            <div class="comment-inner">
                                                                <div class="comment-info clearfix">
                                                                    <strong>{{ $comment1->user()->first()->name }} </strong>
                                                                    <div class="comment-time">
                                                                        {{ $createTime1 }}
                                                                    </div>
                                                                </div>
                                                                <div class="text">
                                                                    {{ $comment1->context }}
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach
                                    <!--Send Comment -->
                                        @if( auth()->check())
                                            <div class="margin-bottom-15 margin-top-15 padding-5">
                                                <h3 class="margin-bottom-15 font-sm">نظر خود را ثبت کنید .</h3>
                                                <form action="{{ url('/create/comment/'.$product->id) }}" method="post">
                                                    {{ csrf_field() }}
                                                    <div class="form-group">
                                                        <textarea name="context" rows="8"
                                                                  class="form-control bg-silver-light"
                                                                  placeholder="متن نظر خود را وارد کنید"></textarea>
                                                        @if ($errors -> has('context'))
                                                            <span class="error red">
                                                            {{ $errors -> first('context') }}
                                                            </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group margin-top-10">
                                                        <button type="submit" class="btn btn-base success-light">ثبت
                                                            نظر
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <!-- End edite codes  -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="panel margin-bottom-25">
                            <div class="panel-heading">
                                <h2 class="font-lg">مطالب مرتبط</h2>
                            </div>
                            <div class="panel-body products ltr">
                                <div class="slickSlider  slider">
                                    @foreach($products as $product1)
                                        @php
                                            if ($counter == 6) break;
                                            if ($product1->id == $product->id) continue;
                                            if ($product1->pic1 != '0'){
                                                $picture = asset('storage/img/users/userid_'.$product1->user_id.'/thumbnail/'.$product1->pic1);
                                            }elseif($product1->pic2 != '0'){
                                                $picture = asset('storage/img/users/userid_'.$product1->user_id.'/thumbnail/'.$product1->pic2);
                                            }elseif($product1->pic3 != '0'){
                                                $picture = asset('storage/img/users/userid_'.$product1->user_id.'/thumbnail/'.$product1->pic3);
                                            }elseif($product1->pic4 != '0'){
                                                $picture = asset('storage/img/users/userid_'.$product1->user_id.'/thumbnail/'.$product1->pic4);
                                            }else{
                                                $picture = asset('storage/img/products/1.jpg');
                                            }
                                        @endphp
                                        <div class="col-xs-6 col-sm-4 col-md-3">
                                            <a href="{{ url('/details/'.$product1->id.'/'.$product1->slug) }}"
                                               class="items">
                                                <figure>
                                                    <img src="{{ $picture }}" class="img-responsive"
                                                         alt="">
                                                </figure>
                                                <div class="text-center">
                                                    <h4 class="font-sm-2">{{ $product1->title }}</h4>
                                                </div>
                                                <div class="price text-center">
                                                    <span>{{ number_format($product1->price) .' '.__('app.toman') }}
                                                        تومان</span>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection