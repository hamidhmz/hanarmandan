@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/site/app.css') }}">
@endsection
@section('js')
@endsection
@section('content')
    @php
        $v = new \Verta($user->created_at);
        if ($user->background_banner == null || $user->background_banner == '0'){
            $banner = 'storage/img/store/18.jpg';
        }else{
            $banner = 'storage/img/users/userid_'.$user->id.'/'.$user->background_banner;
        }
        if ($user->avatar == null || $user->avatar == '0'){
            $avatar = 'storage/img/store/agent1.jpg';
        }else{
            $avatar = 'storage/img/users/userid_'.$user->id.'/'.$user->avatar;
        }
    @endphp
    <div class="main-store">
        <!-- header full baner -->
        <div class="store-full-baner" style="background-image: url('{{asset($banner)}}');">
            <div class="shadow-bottom-to-top"></div>
        </div>
        <div class="top-store-details">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="show-detial">
                            <figure><img src="{{ asset($avatar) }}" alt=""></figure>
                            <div class="wrap">
                                <div class="content-wrap">
                                    <h4 class="pull-right margin-right-20 mb-0 text-white">
                                        {{ $user->name.' '.$user->family_name }}
                                        <span><i class="fa fa-map-marker margin-left-5"></i>{{ $user->store_address }} </span>
                                    </h4>
                                    <a href="tel:{{ $user->phone_number }}" class="pull-left tel text-white">
                                        <i class="fa fa-phone-square margin-left-5"></i> {{ $user->phone_number }}</a>
                                    <div class="clearfix"></div>
                                </div>
                                <!--content-->
                                <div class="bottom-dec">
                                    <div><a href="#"> <i class="fa fa-envelope margin-left-5"></i>
                                            {{ $user->email }}</a>
                                    </div>
                                    <div style=""><i class="fa fa-calendar margin-left-5"></i><span class="font-sm-2 purple">تاریخ ثبت: </span>
                                        {{ $v->format('Y/m/d') }}
                                    </div>
                                    <div>
                                        <ul class="social-icons pull-right">
                                            <li>
                                                <a href="{{ $user->telegram_id }}" data-toggle="tooltip" data-placement="top" title="تلگرام"><i
                                                            class="fa fa-telegram"></i></a>
                                            </li>
                                            <li>
                                                <a href="{{ $user->twitter_id }}" data-toggle="tooltip" data-placement="top" title="توییتر"><i
                                                            class="fa fa-twitter"></i></a>
                                            </li>
                                            <li>
                                                <a href="{{ $user->instagram_id }}" data-toggle="tooltip" data-placement="top"
                                                   title="اینستاگرام"><i
                                                            class="fa fa-instagram "></i></a>
                                            </li>
                                            <li>
                                                <a href="{{ $user->linkdin_id }}" data-toggle="tooltip" data-placement="top"
                                                   title="لینکدین"><i
                                                            class="fa fa-linkedin"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--left block-->
                    <!--sidebar-->
                </div>
            </div>
        </div>
        <!-- .\ header full baner -->
    </div>
    <!-- show store products -->
    <section class=" container margin-top-50 margin-bottom-50">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="font-md dis-inline">محصولات فروشگاه {{ $user->name }} </h3>
                        <span class="pull-left">{{ $products->count() }} مورد یافت شد </span>
                    </div>
                    <div class="panel-body">
                        <div class="row store-products">
                            @foreach($products as $product)
                                @php
                                    if ($product->pic1 != '0'){
                                        $picture = asset('storage/img/users/userid_'.$product->user_id.'/thumbnail/'.$product->pic1);
                                    }elseif($product->pic2 != '0'){
                                        $picture = asset('storage/img/users/userid_'.$product->user_id.'/thumbnail/'.$product->pic2);
                                    }elseif($product->pic3 != '0'){
                                        $picture = asset('storage/img/users/userid_'.$product->user_id.'/thumbnail/'.$product->pic3);
                                    }elseif($product->pic4 != '0'){
                                        $picture = asset('storage/img/users/userid_'.$product->user_id.'/thumbnail/'.$product->pic4);
                                    }else{
                                        $picture = asset('storage/img/products/1.jpg');
                                    }
                                @endphp
                                <div class="col-xs-6 col-sm-4 col-md-3">
                                    <a href="{{ url('/details/'.$product->id.'/'.$product->slug) }}" class="items">
                                        <figure>
                                            <img src="{{ $picture }}" class="img-responsive"
                                                 alt="">
                                        </figure>
                                        <div class="text-center">
                                            <h4 class="font-sm-2">{{ $product->title }}</h4>
                                        </div>
                                        <div class="price text-center">
                                            <span>{{ number_format($product->price) .' '.__('app.toman') }} تومان</span>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- .\ show store products -->
@endsection