@extends('layouts.main')
@section('content')
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-4 ">
                <div class="home-right">
                    <h1 class="font-600 font-md">ققنوس! پایگاه خرید و فروش بی‌واسطه‌</h1>

                    <h3 class="font-600 font-sm-2">اگه دنبال چیزی هستی، شهرت رو انتخاب کن و تو دسته‌بندی‌ها به دنبالش
                        بگرد.</h3>

                    <p class="font-sm">اگر هم می‌خوای چیزی بفروشی، چند تا عکس خوب ازش بگیر و آگهیت رو بچسبون به
                        ققنوس.</p>

                    <div class="margin-top-30 margin-bottom-20 text-center">
                        <img src="{{asset('storage/img/namd.png')}}" alt="">
                    </div>
                    <hr>
                    <div class="margin-top-20 margin-bottom-10 text-center">
                        <strong class="dis-blok margin-top-10 margin-bottom-10">ققنوس را در شبکه‌های اجتماعی دنبال
                            کنید:</strong>
                        <a href="" class="btn btn-xs danger-light">
                            <i class="fa fa-telegram"></i>
                        </a>
                        <a href="" class="btn btn-xs danger-light">
                            <i class="fa fa-instagram"></i>
                        </a>
                        <a href="" class="btn btn-xs danger-light">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="" class="btn btn-xs danger-light">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="home-link">
                    <div role="form">
                        <div class="form-group">
                            <input id="filter" class="form-control" type="search"
                                   placeholder="جستجوی سریع نام شهر ..."/>
                        </div>
                        <ul id="list" class="lists">
                            @foreach($cities as $city)
                            <li>
                                <a class="btn items " title="{{ $city->name }}" href="{{ url('/search').'/'.str_replace(' ','-',$city->name).'/brows' }}">
                                    <span>{{ str_limit($city->name, 10) }}</span>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection