@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/site/vendor/dropzone.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/site/vendor/select2.min.css')}}">
@endsection
@section('js')
    <script type="text/javascript" src="{{asset('js/site/dropzone.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/site/select2.min.js')}}"></script>
@endsection
@section('content')
    <div class="margin-top-50">
        <div class="min-container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>ارسال رایگان آگهی </h1>
                    <div class="category-selector">
                        <a href="{{ url()->previous() }}">
                            <span>{{ str_replace('-',' ',$category).'/'.str_replace('-',' ',$parent_category) }}</span>
                            <strong>تغییر دسته </strong>
                        </a>
                    </div>
                    <form action="{{ url('/create/user-and-product') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="category" value="{{ str_replace('-',' ',$parent_category) }}">
                        <input type="hidden" name="parent_category" value="{{ str_replace('-',' ',$category) }}">
                        <div class="panel">
                            <div class="panel-heading">
                            </div>
                            <div class="panel-body">
                                <div class="ads-filds">
                                    <div>
                                        {{-- sing up --}}
                                        @if(!auth()->check())
                                            <div class="row">
                                                <div class="send-add pc ">
                                                    <a class="btn btn-primary" href="{{ url('/login') }}"> ورود </a>
                                                </div>
                                            </div>
                                            <div class="row margin-top-10">
                                                <div class="col-sm-6">
                                                    <label>نام هنرمند یا فروشگاه : </label>
                                                    <input type="text" name="name" placeholder="نام خود را وارد کنید "
                                                           class="form-control" value="{{ old('name') }}">
                                                    @if ($errors -> has('name'))
                                                        <span class="error red">
                                                    {{ $errors -> first('name') }}
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label>نام خانوادگی هنرمند : </label>
                                                    <input type="text" name="family_name"
                                                           placeholder="نام خانوادگی خود را وارد کنید "
                                                           class="form-control"
                                                           value="{{ old('family_name') }}">
                                                    @if ($errors -> has('family_name'))
                                                        <span class="error red">
                                                    {{ $errors -> first('family_name') }}
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <hr class="margin-top-10 margin-bottom-5">
                                            <div class="row margin-top-10">
                                                <div class="col-sm-4">
                                                    <label>شماره تماس : </label>
                                                    <input type="text" name="phone_number"
                                                           placeholder="شماره تماس خود را وارد کنید "
                                                           class="form-control"
                                                           value="{{ old('phone_number') }}">
                                                    @if ($errors -> has('phone_number'))
                                                        <span class="error red">
                                                    {{ $errors -> first('phone_number') }}
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>آدرس وب سایت : </label>
                                                    <input type="text" name="site_address" placeholder="آدرس وب سایت "
                                                           class="form-control" value="{{ old('site_address') }}">
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>آدرس ایمیل : </label>
                                                    <input type="text" name="email"
                                                           placeholder="آدرس ایمیل خود را وارد کنید "
                                                           class="form-control"
                                                           value="{{ old('email') }}">
                                                    @if ($errors -> has('email'))
                                                        <span class="error red">
                                                    {{ $errors -> first('email') }}
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row margin-top-10">
                                                <div class="col-sm-6">
                                                    <label>ای دی تلگرام : </label>
                                                    <input type="text" name="telegram_id"
                                                           placeholder="ای دی تلگرام خود را وارد کنید "
                                                           class="form-control"
                                                           value="{{ old('telegram_id') }}">
                                                    @if ($errors -> has('telegram_id'))
                                                        <span class="error red">
                                                    {{ $errors -> first('telegram_id') }}
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label>رمز عبور : </label>
                                                    <input type="text" name="password"
                                                           placeholder="رمز عبور خود را انتخاب کنید "
                                                           class="form-control"
                                                           value="{{ old('password') }}">
                                                    @if ($errors -> has('password'))
                                                        <span class="error red">
                                                    {{ $errors -> first('password') }}
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <hr class="margin-top-10 margin-bottom-5">
                                            <div class="row margin-top-10 padding-5">
                                                <div class="col-sm-6">
                                                    <label>طراحی و تولید سفارشی محصولات : </label>
                                                    <div class="margin-right-30 margin-top-10">
                                                        <input type="radio" id="radio1"
                                                               name="custom_design_and_production"
                                                               value="1" placeholder=""
                                                               class="">
                                                        <label for="radio1">دارم</label>
                                                    </div>
                                                    <div class="margin-right-30">
                                                        <input type="radio" id="radio2"
                                                               name="custom_design_and_production"
                                                               value="0" placeholder=""
                                                               class="" checked>
                                                        <label for="radio2">ندارم</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label> تدریس و آموزش : </label>
                                                    <div class="margin-right-30 margin-top-10">
                                                        <input type="radio" id="tadris1" name="teaching_and_training"
                                                               value="1" placeholder=""
                                                               class="">
                                                        <label for="tadris1">دارم</label>
                                                    </div>
                                                    <div class="margin-right-30">
                                                        <input type="radio" id="tadris2" name="teaching_and_training"
                                                               value="0" placeholder=""
                                                               class="" checked>
                                                        <label for="tadris2">ندارم</label>
                                                    </div>
                                                </div>

                                            </div>
                                            <hr class="margin-top-10 margin-bottom-5">
                                            <div class="row margin-top-10 padding-5">
                                                <div class="col-sm-6">
                                                    <label>برگزاری کلاس آموزش آنلاین: </label>
                                                    <div class="margin-right-30 margin-top-10">
                                                        <input type="radio" id="class-dars1" value="1" name="e_learning"
                                                               placeholder=""
                                                               class="" checked>
                                                        <label for="class-dars1">دارم</label>
                                                    </div>
                                                    <div class="margin-right-30">
                                                        <input type="radio" id="class-dars2" value="0" name="e_learning"
                                                               placeholder=""
                                                               class="" checked>
                                                        <label for="class-dars2">ندارم</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label> برنامه آموزشی : </label>
                                                    <div class="margin-right-30 margin-top-10">
                                                        <input type="radio" id="plans1" value="1" name="teach_learning_plan"
                                                               placeholder=""
                                                               class="" checked>
                                                        <label for="plans1">دارم</label>
                                                    </div>
                                                    <div class="margin-right-30">
                                                        <input type="radio" id="plans2" value="0" name="teach_learning_plan"
                                                               placeholder=""
                                                               class="" checked>
                                                        <label for="plans2">ندارم</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class="margin-top-10 margin-bottom-5">
                                            <div class="row margin-top-10 padding-5">
                                                <div class="col-sm-6">
                                                    <label>درجه هنری : </label>
                                                    <div class="margin-right-30 margin-top-10">
                                                        <input type="radio" id="dager-honar1" value="1" name="art_degree"
                                                               placeholder=""
                                                               class="">
                                                        <label for="dager-honar1">دارم</label>
                                                    </div>
                                                    <div class="margin-right-30">
                                                        <input type="radio" id="dager-honar2" value="0" name="art_degree"
                                                               placeholder=""
                                                               class="" checked>
                                                        <label for="dager-honar2" >ندارم</label>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        {{-- end sing up --}}
                                        <hr class="margin-top-10 margin-bottom-5">
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 margin-top-5">
                                                    <label>عنوان محصول : </label>
                                                    <input type="text" name="product_title"
                                                           placeholder="عنوان محصول را وارد کنید " class="form-control"
                                                           value="{{ old('product_title') }}">
                                                    @if ($errors -> has('product_title'))
                                                        <span class="error red">
                                                        {{ $errors -> first('product_title') }}
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="col-sm-12">
                                                    <label>نام محصول : </label>
                                                    <input type="text" name="product_name"
                                                           placeholder="نام محصول خود را وارد کنید "
                                                           class="form-control" value="{{ old('product_name') }}">
                                                    @if ($errors -> has('product_name'))
                                                        <span class="error red">
                                                        {{ $errors -> first('product_name') }}
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-12 margin-top-5">
                                                    <label>تعداد موجودی : </label>
                                                    <input type="text" name="product_number"
                                                           placeholder="تعداد محصول را وارد کنید " class="form-control"
                                                           value="{{ old('product_number') }}">
                                                    @if ($errors -> has('product_number'))
                                                        <span class="error red">
                                                        {{ $errors -> first('product_number') }}
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>جزییات محصول : </label>
                                                <textarea name="product_caption" rows="5"
                                                          placeholder="جزییات محصول را وارد کنید "
                                                          class="form-control">{{ old('product_caption') }}</textarea>
                                                @if ($errors -> has('product_caption'))
                                                    <span class="error red">
                                                        {{ $errors -> first('product_caption') }}
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-6">
                                                    <label>شهر شما : </label>
                                                    <select name="city_name"
                                                            class="form-control browser-default js-example-basic-single">
                                                        @php
                                                            if (null !== (old('city_name'))){
                                                                echo '<option selected="selected" value="'.old('city_name').'">'.old('city_name').'</option>';
                                                            }else{
                                                                echo '<option selected="selected" value="">انتخاب شهر شما</option>';
                                                            }
                                                        @endphp
                                                        @foreach($cities as $city)
                                                            <option value="{{ $city->name }}">{{ $city->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors -> has('city_name'))
                                                        <span class="error red">
                                                        {{ $errors -> first('city_name') }}
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6 margin-top-5">
                                                    <label>قیمت محصول :(به تومان) </label>
                                                    <input type="text" name="product_price"
                                                           placeholder="قیمت محصول را وارد کنید " class="form-control"
                                                           value="{{ old('product_price') }}">
                                                    @if ($errors -> has('product_price'))
                                                        <span class="error red">
                                                        {{ $errors -> first('product_price') }}
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 margin-top-5">
                                                    <label>آدرس محصول : </label>
                                                    <input type="text" name="location_address"
                                                           placeholder="آدرس محصول را وارد کنید " class="form-control"
                                                           value="{{ old('location_address') }}">
                                                    @if ($errors -> has('location_address'))
                                                        <span class="error red">
                                                        {{ $errors -> first('location_address') }}
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="margin-top-10 margin-bottom-5">
                                        <div class=" margin-top-10 padding-5">
                                            <label>ورود تصاویر محصول : </label>
                                            <article>
                                                {{--<strong class="red dis-blok margin-bottom-5"> بهتر است اندازه تصاویر x--}}
                                                {{--باشد</strong>--}}
                                                {{--<strong class="red dis-blok margin-bottom-5"> حداکثر برای هر محصول 4 عکس--}}
                                                {{--میتوانید وارد کنید .</strong>--}}
                                                {{--<strong class="red dis-blok margin-bottom-5">کاربر گرامی تمامی تصاویری--}}
                                                {{--که--}}
                                                {{--میخواهید نمایش دهید را یکجا انتخاب نمایید </strong>--}}
                                                <div class="margin-tb-20">
                                                    <input type="file" name="pic1">
                                                    @if ($errors -> has('pic1'))
                                                        <span class="error red">
                                                        {{ $errors -> first('pic1') }}
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="margin-tb-20">
                                                    <input type="file" name="pic2">
                                                    @if ($errors -> has('pic2'))
                                                        <span class="error red">
                                                        {{ $errors -> first('pic2') }}
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="margin-tb-20">
                                                    <input type="file" name="pic3">
                                                    @if ($errors -> has('pic3'))
                                                        <span class="error red">
                                                        {{ $errors -> first('pic3') }}
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="margin-tb-20">
                                                    <input type="file" name="pic4">
                                                    @if ($errors -> has('pic4'))
                                                        <span class="error red">
                                                        {{ $errors -> first('pic4') }}
                                                        </span>
                                                    @endif
                                                </div>
                                                <div id="dropzone" class="no-margin no-padding">
                                                    <div class="dz-message needsclick">
                                                        برای وارد کردن تصاویر خود کلیک نمایید ..
                                                        <br>
                                                        <span class="note needsclick">تمامی عکس هایی که میخواهید آپلود کنید را انتخاب نمایید .</span>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>

                                        <hr class="margin-top-10 margin-bottom-5">
                                        <div class=" margin-top-10 padding-5">
                                            <label>ارسال فیلم مربوط به محصول (اختیاری): </label>
                                            <div class="margin-tb-20">
                                                <input type="file" name="video">
                                                @if ($errors -> has('video'))
                                                    <span class="error red">
                                                    {{ $errors -> first('video') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <input type="submit" class="btn btn-primary" value="ارسال">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection