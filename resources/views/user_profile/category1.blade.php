@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/site/vendor/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/site/vendor/bootstrap-slider.css')}}">
@endsection
@section('js')
    <script type="text/javascript" src="{{asset('js/site/bootstrap-slider.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/site/select2.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(function () { $('.collapse').collapse('show') });

            $(".js-example-basic-multiple").select2({
                placeholder: "براساس گروه هنری ",
                allowClear: true
            });

            $(".js-example-basic-multiple2").select2({
                placeholder: "انتخاب هنرمند ..  ",
                allowClear: true
            });
        });
        // $("#ex2").slider({});

        // Without JQuery
        var slider = new Slider('#ex2', {});
    </script>
@endsection
@section('content')
    <div class="margin-top-50">
        <div class="min-container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h1>ثبت محصول جدید </h1>
                        </div>
                        <div class="panel-body">
                            <div class="lists-cat">
                                <ul>
                                    @foreach($parent_categories as $category)
                                    <li>
                                        <a href="{{ url('/new/category/second').'/'.$category->slug }}">{{ $category->name }}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection