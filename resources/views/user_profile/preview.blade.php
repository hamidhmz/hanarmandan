@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/site/vendor/dropzone.css')}}">
@endsection
@section('js')
    <script type="text/javascript" src="{{asset('js/site/dropzone.min.js')}}"></script>
    <script type="text/javascript">
        $('.carousel').carousel({
            interval: 4000
        });
    </script>
@endsection
@section('content')
    <div class="margin-top-50">
        <div class="min-container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="stepsbar-wrap">
                        <div id="stepsbar" class="stepsbar">
                            <ol>
                                <li class="done"><span>مشخصات ثبت شد</span></li>

                                <li class="active"><span>تایید شماره تماس </span></li>
                                <li><span>در انتظار بررسی</span></li>
                                <li><span>انتشار</span></li>
                            </ol>
                        </div>
                    </div>
                    <div class="margin-top-5 number-valid text-center">
                        <p>پیام کوتاهی شامل کد به منظور تایید شماره تماس شما فرستاده خواهد شد. لطفا کد را در اینجا وارد
                            کنید</p>
                        <form class="margin-top-10 margin-bottom-10">
                            <input type="text" name="" placeholder="کد تایید را وارد کنید "><br>
                            <button type="" class="btn btn-base info-light margin-top-15">تایید</button>
                        </form>
                        <a href="" class="">ارسال دوباره کد </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- start tabs -->

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">

                <ul id="myTab" class="nav nav-tabs">

                    <li class="active"><a class="font-md" href="#perview" data-toggle="tab">پیش نمایش</a></li>
                    <li><a class="font-md" href="#edit" data-toggle="tab">ویرایش</a></li>
                    <li><a class="font-md" href="#upgrad" data-toggle="tab">ارتقا</a></li>
                    <li><a class="font-md" href="#delete" data-toggle="tab">حذف</a></li>


                </ul>

                <div id="myTabContent" class="tab-content container">

                    <!-- Start perview codes  -->
                    <div class="tab-pane fade in active" id="perview">
                        <div class="perview">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="item-boxs">
                                        <div class="title">
                                            {{ $product->title }}
                                        </div>
                                        <div class="body-box">
                                            <h1 class="font-md">{{ $product->name }}</h1>
                                            <span class="time">
												ایجاد شده در : <small
                                                        style="direction: ltr">{{ $v->hour.':'.$v->minute.':'.$v->second.'  '.$v->year.'/'.$v->month.'/'.$v->day }} </small>
											</span>
                                        </div>
                                    </div>
                                    <div class="item-boxs">
                                        <div class="title">
                                            جزییات
                                        </div>
                                        <div class="body-box">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <ul>
                                                        <li><strong>ایمیل
                                                                : </strong> {{ $product->user()->first()->email }}</li>
                                                        <li><strong>محل : </strong> {{ $product->location_address }}
                                                        </li>
                                                        <li><strong>قیمت
                                                                : </strong> {{ number_format($product->price) }} تومان
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-6">
                                                    <ul>
                                                        <li><strong>تلفن
                                                                : </strong> {{ $product->user()->first()->phone_number }}
                                                        </li>
                                                        {{--<li><strong>نوع : </strong> فروشی</li>--}}
                                                        <li><strong>دسته بندی
                                                                : </strong>{{ $product->parent_category()->first()->name.'/'.$product->category()->first()->name }}
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="item-boxs">
                                        <div class="title">
                                            توضیحات
                                        </div>
                                        <div class="body-box">
                                            <p>{{ $product->caption }} </p>
                                        </div>
                                    </div>
                                </div>

                                <!-- perviw image -->
                                <div class="col-md-5">
                                    <div class="perview-img margin-top-15">

                                        <div id="myCarousel" class="carousel slide">

                                            <!-- Carousel indicators -->
                                            <ol class="carousel-indicators">
                                                @php
                                                    $counter = 0;
                                                @endphp
                                                @if(($product->pic1) != '0')
                                                    <li data-target="#myCarousel" data-slide-to="{{ $counter }}"
                                                        @if($counter == 0)class="active"@endif></li>
                                                    @php
                                                        $counter++;
                                                    @endphp
                                                @endif
                                                @if(($product->pic2) != '0')
                                                    <li data-target="#myCarousel" data-slide-to="{{ $counter }}"
                                                        @if($counter == 0)class="active"@endif></li>
                                                    @php
                                                        $counter++;
                                                    @endphp
                                                @endif
                                                @if(($product->pic3) != '0')
                                                    <li data-target="#myCarousel" data-slide-to="{{ $counter }}"
                                                        @if($counter == 0)class="active"@endif></li>
                                                    @php
                                                        $counter++;
                                                    @endphp
                                                @endif
                                                @if(($product->pic4) != '0')
                                                    <li data-target="#myCarousel" data-slide-to="{{ $counter }}"
                                                        @if($counter == 0)class="active"@endif></li>
                                                    @php
                                                        $counter++;
                                                    @endphp
                                                @endif
                                            </ol>

                                            <!-- Carousel items -->
                                            <div class="carousel-inner">
                                                @php
                                                    $counter = 0;
                                                    $active = '';
                                                @endphp
                                                @if(($product->pic1) != '0')
                                                    @php
                                                        if($counter == 0) $active = 'active';
                                                    @endphp
                                                    <div class="item {{ $active }}">
                                                        <img src="{{asset('storage/img/users/userid_'.auth()->user()->id.'/'.$product->pic1)}}"
                                                             alt="{{ $counter }} slide">
                                                    </div>
                                                    @php
                                                        $active = '';
                                                        $counter++;
                                                    @endphp
                                                @endif
                                                @if(($product->pic2) != '0')
                                                    @php
                                                        if($counter == 0) $active = 'active';
                                                    @endphp
                                                    <div class="item {{ $active }}">
                                                        <img src="{{asset('storage/img/users/userid_'.auth()->user()->id.'/'.$product->pic2)}}"
                                                             alt="{{ $counter }} slide">
                                                    </div>
                                                    @php
                                                        $active = '';
                                                        $counter++;
                                                    @endphp
                                                @endif
                                                @if(($product->pic3) != '0')
                                                    @php
                                                        if($counter == 0) $active = 'active';
                                                    @endphp
                                                    <div class="item {{ $active }}">
                                                        <img src="{{asset('storage/img/users/userid_'.auth()->user()->id.'/'.$product->pic3)}}"
                                                             alt="{{ $counter }} slide">
                                                    </div>
                                                    @php
                                                        $active = '';
                                                        $counter++;
                                                    @endphp
                                                @endif
                                                @if(($product->pic4) != '0')
                                                    @php
                                                        if($counter == 0) $active = 'active';
                                                    @endphp
                                                    <div class="item {{ $active }}">
                                                        <img src="{{asset('storage/img/users/userid_'.auth()->user()->id.'/'.$product->pic4)}}"
                                                             alt="{{ $counter }} slide">
                                                    </div>
                                                    @php
                                                        $active = '';
                                                        $counter++;
                                                    @endphp
                                                @endif
                                            </div>

                                            <!-- Carousel nav -->
                                            <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                                            <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- End perview codes  -->

                    <!-- Start edite codes  -->
                    <div class="tab-pane fade" id="edit">
                        <div class=" margin-top-25">
                            <div class="row">
                                <div class="col-md-12 edit">
                                    <hr class="margin-top-10 margin-bottom-5">
                                    <form action="{{ url('/edit/product/details/'.$product->id) }}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 margin-top-5">
                                                    <label>عنوان محصول : </label>
                                                    <input type="text" name="product_title"
                                                           placeholder="عنوان محصول را وارد کنید " class="form-control"
                                                           value="{{ $product->title }}">
                                                    @if ($errors -> has('product_title'))
                                                        <span class="error red">
                                                            {{ $errors -> first('product_title') }}
                                                            </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="col-sm-12">
                                                    <label>نام محصول : </label>
                                                    <input type="text" name="product_name"
                                                           placeholder="نام محصول خود را وارد کنید "
                                                           class="form-control" value="{{ $product->name }}"
                                                           disabled="disabled">
                                                    @if ($errors -> has('product_name'))
                                                        <span class="error red">
                                                            {{ $errors -> first('product_name') }}
                                                            </span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-12 margin-top-5">
                                                    <label>تعداد موجودی : </label>
                                                    <input type="text" name="product_number"
                                                           placeholder="تعداد محصول را وارد کنید " class="form-control"
                                                           value="{{ $product->number }}">
                                                    @if ($errors -> has('product_number'))
                                                        <span class="error red">
                                                            {{ $errors -> first('product_number') }}
                                                            </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>جزییات محصول : </label>
                                                <textarea name="product_caption" rows="5"
                                                          placeholder="جزییات محصول را وارد کنید "
                                                          class="form-control">{{ $product->caption }}</textarea>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-6">
                                                    <label>شهر شما : </label>
                                                    <select name="city_name"
                                                            class="form-control browser-default js-example-basic-single">
                                                        <option selected="selected"
                                                                value="{{ $product->city()->first()->name }}">{{ $product->city()->first()->name }}</option>
                                                        @foreach($cities as $city)
                                                            <option value="{{ $city->name }}">{{ $city->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors -> has('city_name'))
                                                        <span class="error red">
                                                            {{ $errors -> first('city_name') }}
                                                            </span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6 margin-top-5">
                                                    <label>قیمت محصول :(به تومان) </label>
                                                    <input type="text" name="product_price"
                                                           placeholder="قیمت محصول را وارد کنید " class="form-control"
                                                           value="{{ $product->price }}">
                                                    @if ($errors -> has('product_price'))
                                                        <span class="error red">
                                                            {{ $errors -> first('product_price') }}
                                                            </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 margin-top-5">
                                                    <label>آدرس محصول : </label>
                                                    <input type="text" name="location_address"
                                                           placeholder="آدرس محصول را وارد کنید " class="form-control"
                                                           value="{{ $product->location_address }}">
                                                    @if ($errors -> has('location_address'))
                                                        <span class="error red">
                                                            {{ $errors -> first('location_address') }}
                                                            </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="margin-top-10 margin-bottom-5">

                                        <div class="row">
                                            <div class="col-md-7">
                                                <div class=" margin-top-10 padding-5">
                                                    <label>حذف تصاویر محصول : </label>
                                                    <div class="row">
                                                        @if($product->pic1 != '0' )
                                                            <div class="col-xs-6 col-sm-4 col-md-6">
                                                                <label>تصویر 1 : </label>
                                                                <div class="edit-img-box margin-tb-20">
                                                                    <img src="{{asset('storage/img/users/userid_'.auth()->user()->id.'/thumbnail/'.$product->pic1)}}"
                                                                         class="img-responsive" alt="">
                                                                    <a href="{{ url('/delete/product-picture/pic1/'.$product->id ) }}"
                                                                       class="del btn btn-xs danger-light"
                                                                       data-toggle="tooltip" data-placement="top"
                                                                       title="حذف تصویر"><i class="fa fa-close"></i></a>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        @if($product->pic2 != '0' )
                                                            <div class="col-xs-6 col-sm-4 col-md-6">
                                                                <label>تصویر 2 : </label>
                                                                <div class="edit-img-box margin-tb-20">
                                                                    <img src="{{asset('storage/img/users/userid_'.auth()->user()->id.'/thumbnail/'.$product->pic2)}}"
                                                                         class="img-responsive" alt="">
                                                                    <a href="{{ url('/delete/product-picture/pic2/'.$product->id ) }}"
                                                                       class="del btn btn-xs danger-light"
                                                                       data-toggle="tooltip" data-placement="top"
                                                                       title="حذف تصویر"><i class="fa fa-close"></i></a>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        @if($product->pic3 != '0' )
                                                            <div class="col-xs-6 col-sm-4 col-md-6">
                                                                <label>تصویر 3 : </label>
                                                                <div class="edit-img-box margin-tb-20">
                                                                    <img src="{{asset('storage/img/users/userid_'.auth()->user()->id.'/thumbnail/'.$product->pic3)}}"
                                                                         class="img-responsive" alt="">
                                                                    <a href="{{ url('/delete/product-picture/pic3/'.$product->id ) }}"
                                                                       class="del btn btn-xs danger-light"
                                                                       data-toggle="tooltip" data-placement="top"
                                                                       title="حذف تصویر"><i class="fa fa-close"></i></a>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        @if($product->pic4 != '0' )
                                                            <div class="col-xs-6 col-sm-4 col-md-6">
                                                                <label>تصویر 4 : </label>
                                                                <div class="edit-img-box margin-tb-20">
                                                                    <img src="{{asset('storage/img/users/userid_'.auth()->user()->id.'/thumbnail/'.$product->pic4)}}"
                                                                         class="img-responsive" alt="">
                                                                    <a href="{{ url('/delete/product-picture/pic3/'.$product->id ) }}"
                                                                       class="del btn btn-xs danger-light"
                                                                       data-toggle="tooltip" data-placement="top"
                                                                       title="حذف تصویر"><i class="fa fa-close"></i></a>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class=" margin-top-10 padding-5">
                                                    <label>ورود تصاویر محصول : </label>
                                                    <article>
                                                        <div class="margin-tb-20">
                                                            <label>تصویر 1 : </label>
                                                            <input type="file" name="pic1">
                                                            @if ($errors -> has('pic1'))
                                                                <span class="error red">
                                                            {{ $errors -> first('pic1') }}
                                                            </span>
                                                            @endif
                                                        </div>
                                                        <div class="margin-tb-20">
                                                            <label>تصویر 2 : </label>
                                                            <input type="file" name="pic2">
                                                            @if ($errors -> has('pic2'))
                                                                <span class="error red">
                                                            {{ $errors -> first('pic2') }}
                                                            </span>
                                                            @endif
                                                        </div>
                                                        <div class="margin-tb-20">
                                                            <label>تصویر 3 : </label>
                                                            <input type="file" name="pic3">
                                                            @if ($errors -> has('pic3'))
                                                                <span class="error red">
                                                            {{ $errors -> first('pic3') }}
                                                            </span>
                                                            @endif
                                                        </div>
                                                        <div class="margin-tb-20">
                                                            <label>تصویر 4 : </label>
                                                            <input type="file" name="pic4">
                                                            @if ($errors -> has('pic4'))
                                                                <span class="error red">
                                                            {{ $errors -> first('pic4') }}
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="margin-top-10 margin-bottom-5">
                                        @if($product->video != '0' )
                                            <div class="col-xs-6 col-sm-4 col-md-6">
                                                <a href="{{ url('/delete/product-video/'.$product->id ) }}"
                                                   class="del btn btn-xs danger-light"
                                                   data-toggle="tooltip" data-placement="top"
                                                   title="حذف فیلم"><i class="fa fa-close"></i></a>

                                                <video width="400" controls>
                                                    <source src="{{asset('storage/video/users/userid_'.auth()->user()->id.'/'.$product->video)}}" >
                                                </video>
                                            </div>
                                        @endif
                                        <div class=" margin-top-10 padding-5">
                                            <label>ارسال فیلم مربوط به محصول (اختیاری): </label>
                                            <div class="margin-tb-20">
                                                <input type="file" name="video">
                                                @if ($errors -> has('video'))
                                                    <span class="error red">
                                                        {{ $errors -> first('video') }}
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <input type="submit" class="btn btn-primary" value="ارسال">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End edite codes  -->

                    <!-- Start upgrad codes  -->
                    <div class="tab-pane fade" id="upgrad">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="margin-top-50 margin-bottom-50 bg-white ">
                                    <table class="table table-bordered">
                                        <thead>
                                        <th>انتخاب</th>
                                        <th>عنوان طرح</th>
                                        <th>مبلغ</th>
                                        <th>وضعیت</th>
                                        <th>کد هدیه</th>
                                        <th>توضیحات</th>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="">
                                            </td>
                                            <td>اسم طرح</td>
                                            <td>10000 تومان</td>
                                            <td>
                                                <span class="label label-danger">پرداخت نشده</span>
                                            </td>
                                            <td>کد هدیه</td>
                                            <td>
                                                <p>بعد از انتشار آگهی این هزینه قابل پرداخت است

                                                    آگهی شما تا زمان دریافت آگهی تازه‌تر در همان دسته‌بندی و شهر، به
                                                    عنوان اولین آگهی نمایش داده می‌شود.
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="">
                                            </td>
                                            <td>اسم طرح</td>
                                            <td>10000 تومان</td>
                                            <td>
                                                <span class="label label-success">پرداخت شده</span>
                                            </td>
                                            <td>کد هدیه</td>
                                            <td>
                                                <p>بعد از انتشار آگهی این هزینه قابل پرداخت است

                                                    آگهی شما تا زمان دریافت آگهی تازه‌تر در همان دسته‌بندی و شهر، به
                                                    عنوان اولین آگهی نمایش داده می‌شود.
                                                </p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End upgrad codes  -->

                    <!-- Start delete codes  -->
                    <div class="tab-pane fade" id="delete">
                        <div class="row">
                            <div class="col-xs-12">
                                <strong class="dis-blok font-md margin-top-30">آیا میخواهید این آگهی حذف شود ؟ </strong>
                                <br>
                                <a href="{{ url('/delete/product/'.$product->id) }}" class="btn btn-base danger-light">بله ، حذف شود </a>
                            </div>
                        </div>
                    </div>
                    <!-- End delete codes  -->

                </div>


            </div>
        </div>
    </div>
@endsection