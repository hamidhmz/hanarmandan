@extends('layouts.main')
@section('css')
@endsection
@section('js')
    <script type="text/javascript" src="{{asset('js/site/share-button.js')}}"></script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection
@section('content')
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="my-qoqnos rounded-3">
                        <div class="my-qoqnos-list">
                            <ul class="row">
                                <li><a href="">آگهیهای من</a></li>
                                <li class="active"><a href="">آگهیهای نشان شده</a></li>
                                <!-- <li><a href="">بازدیدهای اخیر</a></li> -->
                                <li><a href="">ورود به پنل کسب و کار</a></li>
                            </ul>
                        </div>
                        <div class="my-qoqnos-body">
                            <div class="alerted">
                                <p>هیچ آگهی‌ای نشان نشده است</p>
                            </div>

                            <div class="products row">
                                <a href=""
                                   class="margin-top-10 margin-bottom-10 margin-right-15 btn btn-base success-light">حذف
                                    تاریخچه</a>
                                <div class="alerted">
                                    <p>برای مشاهده‌ی جزییات هر آگهی ، روی آن کلیک نمایید .</p>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="items row">
                                        <a href="">
                                            <div class="col-xs-6">
                                                <strong class="dis-blok margin-top-5">نام محصول عنوان</strong>
                                                <div class="bottom">
                                                    <p class="dis-blok">محله یا شهر مورد نظر</p>
                                                    <small class="dis-blok">تاریخ انتشار</small>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 no-padding">
                                                <div class="item-img">
                                                    <img src="{{asset('storage/img/products/1.jpg')}}" class="img-responsive"
                                                         alt="عنوان محصول">
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="items-bottom row">
                                        <div class="col-xs-12">
                                            <div class="share-button" id="share-button1">
                                                <a href="#" class="success-light" onclick="sotioal_show(1)">اشتراک
                                                    گذاری</a>
                                                <div class="icon-wrapper">
                                                    <ul>
                                                        <li><a target="_blank" href="https://twitter.com/share"
                                                               data-url="{{ content.absolute_url }}"
                                                               data-toggle="tooltip" data-placement="top"
                                                               title="اشتراک گذاری در توییتر"><i
                                                                        class="fa fa-twitter"></i></a></li>
                                                        <li><a target="_blank"
                                                               href="https://facebook.com/sharer/sharer.php?"
                                                               data-url="{{ content.absolute_url }}"
                                                               data-toggle="tooltip" data-placement="top"
                                                               title="اشتراک گذاری در فیس بوک"><i
                                                                        class="fa fa-facebook"></i></a></li>
                                                        <li><a target="_blank" href="#"
                                                               onclick="javascript:window.open('https://www.linkedin.com/shareArticle?mini=true&amp;url={{ content.absolute_url }}&amp;title=','popup','width=600,height=600');"
                                                               data-toggle="tooltip" data-placement="top"
                                                               title="اشتراک گذاری در لینکدین"><i
                                                                        class="fa fa-linkedin"></i></a></li>
                                                        <li><a target="_blank"
                                                               href="https://plus.google.com/share?url={{ content.absolute_url }}"
                                                               onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                                                               data-toggle="tooltip" data-placement="top"
                                                               title="اشتراک گذاری در گوگل پلاس"><i
                                                                        class="fa fa-google-plus"></i></a></li>
                                                        <li><a target="_blank" href="" data-toggle="tooltip"
                                                               data-placement="top" title="اشتراک گذاری در تلگرام"><i
                                                                        class="fa fa-telegram"></i></a></li>
                                                        <li><a target="_blank" href="" data-toggle="tooltip"
                                                               data-placement="top"
                                                               title="اشتراک گذاری در اینستاگرام"><i
                                                                        class="fa fa-instagram"></i></a></li>
                                                        <li><a href="#" class="close-social-icons"
                                                               onclick="closeIcons(1)" data-toggle="tooltip"
                                                               data-placement="top" title="بستن "><i
                                                                        class="fa fa-times"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="margin-top-50 text-center dis-blok">
                                                <a href="" class="font-gray2"><i class="fa fa-close"></i> حذف</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="items row">
                                        <a href="">
                                            <div class="col-xs-6">
                                                <strong class="dis-blok margin-top-5">نام محصول عنوان</strong>
                                                <div class="bottom">
                                                    <p class="dis-blok">محله یا شهر مورد نظر</p>
                                                    <small class="dis-blok">تاریخ انتشار</small>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 no-padding">
                                                <div class="item-img">
                                                    <img src="{{asset('storage/img/products/1.jpg')}}" class="img-responsive"
                                                         alt="عنوان محصول">
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="items-bottom row">
                                        <div class="col-xs-12">
                                            <div class="share-button " id="share-button2">
                                                <a href="#" class="success-light" onclick="sotioal_show(2)">اشتراک
                                                    گذاری</a>
                                                <div class="icon-wrapper">
                                                    <ul>
                                                        <li><a target="_blank" href="https://twitter.com/share"
                                                               data-url="{{ content.absolute_url }}"
                                                               data-toggle="tooltip" data-placement="top"
                                                               title="اشتراک گذاری در توییتر"><i
                                                                        class="fa fa-twitter"></i></a></li>
                                                        <li><a target="_blank"
                                                               href="https://facebook.com/sharer/sharer.php?"
                                                               data-url="{{ content.absolute_url }}"
                                                               data-toggle="tooltip" data-placement="top"
                                                               title="اشتراک گذاری در فیس بوک"><i
                                                                        class="fa fa-facebook"></i></a></li>
                                                        <li><a target="_blank" href="#"
                                                               onclick="javascript:window.open('https://www.linkedin.com/shareArticle?mini=true&amp;url={{ content.absolute_url }}&amp;title=','popup','width=600,height=600');"
                                                               data-toggle="tooltip" data-placement="top"
                                                               title="اشتراک گذاری در لینکدین"><i
                                                                        class="fa fa-linkedin"></i></a></li>
                                                        <li><a target="_blank"
                                                               href="https://plus.google.com/share?url={{ content.absolute_url }}"
                                                               onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                                                               data-toggle="tooltip" data-placement="top"
                                                               title="اشتراک گذاری در گوگل پلاس"><i
                                                                        class="fa fa-google-plus"></i></a></li>
                                                        <li><a target="_blank" href="" data-toggle="tooltip"
                                                               data-placement="top" title="اشتراک گذاری در تلگرام"><i
                                                                        class="fa fa-telegram"></i></a></li>
                                                        <li><a target="_blank" href="" data-toggle="tooltip"
                                                               data-placement="top"
                                                               title="اشتراک گذاری در اینستاگرام"><i
                                                                        class="fa fa-instagram"></i></a></li>
                                                        <li><a href="#" class="close-social-icons"
                                                               onclick="closeIcons(2)" data-toggle="tooltip"
                                                               data-placement="top" title="بستن "><i
                                                                        class="fa fa-times"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="margin-top-50 text-center dis-blok">
                                                <a href="" class="font-gray2"><i class="fa fa-close"></i> حذف</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection