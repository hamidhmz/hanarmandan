@extends('layouts.main')
@section('css')
@endsection
@section('js')
@endsection
@section('content')
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="my-qoqnos rounded-3">
                        <div class="my-qoqnos-list">
                            <ul class="row">
                                <li class="active"><a href="">آگهیهای من</a></li>
                                <li><a href="">آگهیهای نشان شده</a></li>
                                <!-- <li><a href="">بازدیدهای اخیر</a></li> -->
                                <li><a href="">ورود به پنل کسب و کار</a></li>
                            </ul>
                        </div>
                        <div class="my-qoqnos-body">
                            <div class="alerted">
                                <p>هیچ آگهی‌ای ذخیره نشده است</p>
                            </div>

                            <div class="products row">
                                <a href=""
                                   class="margin-top-10 margin-bottom-10 margin-right-15 btn btn-base success-light">حذف
                                    تاریخچه</a>
                                <div class="alerted">
                                    <p>برای مشاهده‌ی جزییات هر آگهی ، روی آن کلیک نمایید .</p>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="items row">
                                        <a href="">
                                            <div class="col-xs-6">
                                                <strong class="dis-blok margin-top-5">نام محصول عنوان</strong>
                                                <div class="bottom">
                                                    <p class="dis-blok">محله یا شهر مورد نظر</p>
                                                    <small class="dis-blok">تاریخ انتشار</small>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 no-padding">
                                                <div class="item-img">
                                                    <img src="{{asset('storage/img/products/1.jpg')}}" class="img-responsive"
                                                         alt="عنوان محصول">
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="items-bottom row">
                                        <div class="col-xs-12">
                                            <strong>قیمت :</strong>
                                            <small>توافقی</small>
                                            <div class="margin-top-10 text-center">
                                                <a href="" class="font-gray2"><i class="fa fa-close"></i> حذف</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="items row">
                                        <a href="">
                                            <div class="col-xs-6">
                                                <strong class="dis-blok margin-top-5">نام محصول عنوان</strong>
                                                <div class="bottom">
                                                    <p class="dis-blok">محله یا شهر مورد نظر</p>
                                                    <small class="dis-blok">تاریخ انتشار</small>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 no-padding">
                                                <div class="item-img">
                                                    <img src="{{asset('storage/img/products/1.jpg')}}" class="img-responsive"
                                                         alt="عنوان محصول">
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="items-bottom row">
                                        <div class="col-xs-12">
                                            <strong>قیمت :</strong>
                                            <small>توافقی</small>
                                            <div class="margin-top-10 text-center">
                                                <a href="" class="font-gray2"><i class="fa fa-close"></i> حذف</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="items row">
                                        <a href="">
                                            <div class="col-xs-6">
                                                <strong class="dis-blok margin-top-5">نام محصول عنوان</strong>
                                                <div class="bottom">
                                                    <p class="dis-blok">محله یا شهر مورد نظر</p>
                                                    <small class="dis-blok">تاریخ انتشار</small>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 no-padding">
                                                <div class="item-img">
                                                    <img src="{{asset('storage/img/products/1.jpg')}}" class="img-responsive"
                                                         alt="عنوان محصول">
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="items-bottom row">
                                        <div class="col-xs-12">
                                            <strong>قیمت :</strong>
                                            <small>توافقی</small>
                                            <div class="margin-top-10 text-center">
                                                <a href="" class="font-gray2"><i class="fa fa-close"></i> حذف</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="items row">
                                        <a href="">
                                            <div class="col-xs-6">
                                                <strong class="dis-blok margin-top-5">نام محصول عنوان</strong>
                                                <div class="bottom">
                                                    <p class="dis-blok">محله یا شهر مورد نظر</p>
                                                    <small class="dis-blok">تاریخ انتشار</small>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 no-padding">
                                                <div class="item-img">
                                                    <img src="{{asset('storage/img/products/1.jpg')}}" class="img-responsive"
                                                         alt="عنوان محصول">
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="items-bottom row">
                                        <div class="col-xs-12">
                                            <strong>قیمت :</strong>
                                            <small>توافقی</small>
                                            <div class="margin-top-10 text-center">
                                                <a href="" class="font-gray2"><i class="fa fa-close"></i> حذف</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection