@extends('layouts.main')
@section('css')
@endsection
@section('js')
    <script type="text/javascript" src="{{asset('js/site/share-button.js')}}"></script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection
@section('content')
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="auth">
                        <div class="margin-tb-20 margin-right-5 margin-left-5 alert">
                            <p>برای دسترسی به این بخش باید وارد ققنوس شوید.</p>
                        </div>
                        <div class="margin-top-15 margin-bottom-15 auth-form">
                            <form>
                                <div class="form-group">
                                    <label>98+</label>
                                    <input type="phone" name="" placeholder="9** ** ** ***" class="form-control">
                                </div>
                                <div class="form-group text-center padding-top-15">
                                    <button class="btn btn-base success-dark">دریافت کد تایید</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection